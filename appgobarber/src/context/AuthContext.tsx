import React, { createContext, 
                useCallback, 
                useState, 
                useContext,
                useEffect
} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../services/api';

interface User {
  id: string;
  name: string;
  email: string;
  avatar_url: string;
}

interface AuthState {
  token: string;
  user: User;
}

interface SignInCredentials {
  email: string;
  password: string;
}

interface AuthContextData {
  user: User;
  loading: boolean; 
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
  updateUser(user: User): Promise<void>;
}

//{} as AuthContext - Inicia sempre vazio
const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>({} as AuthState);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function loadStoragedData(): Promise<void> {
      setLoading(true);
      
      const [token, user] = await AsyncStorage.multiGet([
        '@GoBarber:token',
        '@GoBarber:user',
      ]);

      if (token[1] && user[1]){        
        //Seta o Token para que seja utilizado nas requisições para a API
        api.defaults.headers.authorization = `Bearer ${token[1]}`;

        setData({ token: token[1], user: JSON.parse(user[1]) });
      }

      setLoading(false);
    }  

    //Executa a função para carregar os dados do usuário autenticado
    loadStoragedData();
  }, []);

  const signIn = useCallback(async ({ email, password }) => {
    //Realiza a requisição de autenticação para a API, passando o Email e Senha
    //informados no componente SignIn
    const response = await api.post('sessions', {
      email,
      password
    });

    //Captura os dados de retornados na Autenticação e salva no Storage
    const { token, user } = response.data;
    await AsyncStorage.multiSet([
      ['@GoBarber:token', token],
      ['@GoBarber:user', JSON.stringify(user)] //Converte o User em String
    ]);

    //Seta o Token para que seja utilizado nas requisições para a API
    api.defaults.headers.authorization = `Bearer ${token}`;     

    setData({ token, user }); //Atualiza os dados de Autenticação armazenados no State
  }, []);

  const signOut = useCallback(async () => {
    //Exclui as informações do Storage
    await AsyncStorage.multiRemove(['@GoBarber:token',
                                    '@GoBarber:user']);
    setData({} as AuthState);
  }, []);

  //Atualiza os Dados do usuário logado, após a atualização do Profile
  const updateUser = useCallback( async (user: User) => {
    await AsyncStorage.multiSet([
      ['@GoBarber:user', JSON.stringify(user)] //Converte o User em String
    ]);    
    
    setData({
      token: data.token,
      user
    });
  }, [setData, data.token]);  

  return (
    //Disponibiliza as informações user e signIn para qualquer componente que necessite
    //dessa forma, é possível obter os dados do usuário em qualquer parte da aplicação
    <AuthContext.Provider value={{ user: data.user, loading, signIn, signOut, updateUser }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  //Verifica se o contexto foi criado, se o contexto abrange o componente que está chamando
  //assim como está descrito no tópico "3.20 API de Contexto - Compartilhando Estados entre vários Componentes"
  if (!context){
    throw new Error('useAuth não configurado para o APP');
  }

  return context;
}

export { AuthProvider, useAuth };
