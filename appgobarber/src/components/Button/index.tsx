import React from 'react';
import { RectButtonProperties } from 'react-native-gesture-handler';

import { Container, TextButton } from './styles';

//Herda as propriedades de RectButton, para que possa ser passado por parâmetro
//já que o componente Container é um RectButton
interface ButtonProps extends RectButtonProperties {
  //Torna a propriedade children como obrigatória e que só aceita string
  children: string; 
}

const Button: React.FC<ButtonProps> = ({ children, ...rest }) => (
  <Container {...rest}>
    <TextButton>{children}</TextButton>
  </Container> 
);

export default Button;