import React, 
       { useEffect, 
         useState, 
         useRef, 
         useImperativeHandle, 
         forwardRef,
         useCallback
        } from 'react';
import { TextInputProps } from 'react-native';
import { useField } from '@unform/core';

import { Container, TextInput, Icon } from './styles';

interface InputProps extends TextInputProps {
  name: string;
  icon: string; //No React Native é passado o nome do Ícone diferente da Web
  containerStyle?: {}; 
}

interface InputValueReference {
  value: string;
}

interface InputRef {
  focus(): void;
}

const Input: React.ForwardRefRenderFunction<InputRef, InputProps> = (
  { name, icon, containerStyle, ...rest }, 
  ref
) => {
  const inputElementRef = useRef<any>(null);
  const { registerField, defaultValue='', fieldName, error } = useField(name);

  //Cria a referência do Input e seta o valor inicial, o valor que veio do Form, pelo useField
  const inputValueRef = useRef<InputValueReference>({ value: defaultValue });

  const [isFocused, setIsFocused] = useState(false);  //Criado para monitorar quando for setado foco no componente
  const [isFilled, setIsFilled] = useState(false);   

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => { //Quando o Foco sair do campo
    setIsFocused(false);
    setIsFilled(!!inputValueRef.current.value); //Verifica se possui valor
  }, []);  

  useImperativeHandle(ref, () => ({
    focus() {
      //Seta para dentro da função focus() do InputRef a função focus() do elemento ref passado pelo elemento filho
      inputElementRef.current.focus();
    }
  })); 

  useEffect(() => {
    registerField({
      name: fieldName, // Nome do elemento do Form 
      ref: inputValueRef.current, // Define o elemento de onde será pego o valor 
      path: 'value', // Define o campo do elemento Ref que possui o valor do campo 
      setValue(ref: any, value: string){ //Ao alterar o valor do componente na tela, cairá nessa função
        inputValueRef.current.value = value; //Atualiza o valor do inputValueRef 
        inputElementRef.current.setNativeProps({ text: value }); //Atualiza o valor do componente na tela
      },
      clearValue(){ //Quando for limpo o valor do componente pelo unForm
        inputValueRef.current.value = '';
        inputElementRef.current.clear();
      }
    });
  }, [fieldName, registerField]); 

  return (
      <Container  style={containerStyle} isFocused={isFocused} isErrored={!!error}>
        <Icon name={icon} size={20} color={isFocused || isFilled ? '#ff9000' : '#666360'} />

        <TextInput 
          ref={inputElementRef}
          placeholderTextColor="#666360" {...rest} 
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          defaultValue={defaultValue} //Seta o valor inicial para o campo
          onChangeText={(value) => {
            //Captura o valor informado no Input e joga para dentro do elemento inputValueRef
            inputValueRef.current.value = value;
          }}
          {...rest}
        />
      </Container>
    );
};

export default forwardRef(Input);