import { useNavigation, useRoute } from '@react-navigation/core';
import React, { useCallback, useMemo } from 'react';
import { format } from 'date-fns';
import ptBR from 'date-fns/locale/pt-BR';

import Icon from 'react-native-vector-icons/Feather';

import { 
  Container,
  Title,
  Description,
  OkButton,
  OkButtonText
} from './styles';

interface IRouteParams {
  date: number;
}

const AppointmentCreated: React.FC = () => {
  const { reset } = useNavigation();
  const { params } = useRoute();

  //Aplica a "tipagem" no Obj recebido por parâmetro
  const routeParams = params as IRouteParams;

  const handleOkPress = useCallback(() => {
    //Reseta o Histórico de Rotas, para que ao clicar em 
    //concluir, seja aberto a página de Dashboard e não
    //seja possível voltar para essa tela
    reset({
      routes: [{ name: 'Dashboard' }],
      index: 0,
    });
  }, [reset]);

  const formattedDate = useMemo(() => {
    return format(
      routeParams.date,
      "EEEE', dia' dd 'de' MMM 'de' yyyy 'às' HH:mm'h'",
      { locale: ptBR },
    );
  }, [routeParams.date]);

  return (
    <Container>
      <Icon name="check" size={80} color="#04d361" />

      <Title>Agendamento Concluído</Title>
      <Description>{formattedDate}</Description>

      <OkButton onPress={handleOkPress}>
        <OkButtonText>Ok</OkButtonText>
      </OkButton>

    </Container>
  )
};

export default AppointmentCreated;