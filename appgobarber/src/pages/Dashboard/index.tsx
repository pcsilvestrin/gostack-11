import React, { useCallback, useEffect, useState } from 'react';
import { FlatList } from 'react-native-gesture-handler';

import Icon from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/core';
import { useAuth } from '../../context/AuthContext';
import api from '../../services/api';

import { 
  Container, 
  Header, 
  HeaderTitle, 
  UserName,
  ProfileButton,
  UserAvatar,
  //ProvidersList,
  ProviderContainer,
  ProviderAvatar,
  ProviderInfo,
  ProviderName,
  ProviderMeta,
  ProviderMetaText,
  ProviderListTitle
} from './styles';

export interface IProvider {
  id: string;
  name: string;
  avatar_url: string;
}

const Dashboard: React.FC = () => {
  const [providers, setProviders] = useState<IProvider[]>([]);

  const { signOut, user } = useAuth();
  const { navigate } = useNavigation();

  useEffect(() => {  
    api.get('providers').then((response) => {
      setProviders(response.data);
    })
  }, [setProviders]);

  const navigateToProfile = useCallback(() => {
    navigate('Profile');
  }, [navigate]);

  const navigateToCreateAppointment = useCallback((providerId: string) => {
    navigate('CreateAppointment', { providerId });
  }, [navigate]);

  return (
    <Container>
      <Header>
        <HeaderTitle>
          Bem vindo, {"\n"} 
          <UserName>{user.name}</UserName>
        </HeaderTitle>

        <ProfileButton onPress={navigateToProfile}>
          <UserAvatar source={{ uri: user.avatar_url }} />
        </ProfileButton>
      </Header>

      <FlatList 
        data={providers}
        keyExtractor={(provider) => provider.id}
        ListHeaderComponent={
          <ProviderListTitle>Cabeleireiros</ProviderListTitle>
        }
        renderItem={({ item: provider }) => (
          <ProviderContainer onPress={() => navigateToCreateAppointment(provider.id)}> 
            <ProviderAvatar source={{ uri: provider.avatar_url }} />

            <ProviderInfo> 
               <ProviderName>{provider.name}</ProviderName>

               <ProviderMeta>
                 <Icon name="calendar" size={14} color="#ff9000" />
                 <ProviderMetaText>Segunda à Sexta</ProviderMetaText>
               </ProviderMeta>

               <ProviderMeta>
                 <Icon name="clock" size={14} color="#ff9000" />
                 <ProviderMetaText>8h às 18h</ProviderMetaText>
               </ProviderMeta>              

            </ProviderInfo> 
          </ProviderContainer> 
          
        )} 
      />   
      
    </Container>
  )
};

export default Dashboard;