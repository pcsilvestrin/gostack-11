import React, { useRef, useCallback } from 'react';
import { View, 
         ScrollView, 
         KeyboardAvoidingView, 
         Platform,
         TextInput,
         Alert
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

import { useAuth } from '../../context/AuthContext';
import { useNavigation } from '@react-navigation/native';

import { Form } from '@unform/mobile';
import { FormHandles } from '@unform/core';

import * as Yup from 'yup';
import getValidationErrors from '../../utils/getValidationErrors';

import api from '../../services/api';
import Input from '../../components/Input'; 
import Button from '../../components/Button';


import { Container, 
         Title, 
         UserAvatarButton,
         UserAvatar,
         BackButton
} from './styles';


const Profile: React.FC = () => {
  const { user, updateUser } = useAuth();

  //FormHandles - Possibilita manipular as propriedades do Form
  const formRef = useRef<FormHandles>(null);
  const navigation = useNavigation();

  const email = useRef<TextInput>(null);
  const oldPassword = useRef<TextInput>(null);  
  const newPassword = useRef<TextInput>(null);  
  const confirmationPassword = useRef<TextInput>(null);      

  interface ProfileFormData {
    name: string;
    email: string;
    old_password: string;
    password: string;
    password_confirmation: string;
  }

  const handleSignUp = useCallback(async (data: ProfileFormData) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        name: Yup.string().required('Nome obrigatório'),
        email: Yup.string().required('Email obrigatório').email('Digite um e-mail válido'),

        old_password: Yup.string(),

        password: Yup.string().when('old_password', {
          is: (val: string) => !!val.length, //Verifica se 'old_password' possui valor
          then: Yup.string().required('Campo obrigatório!'), //Se 'old_password' possuir valor, 'password' é Obrigatório
          otherwise: Yup.string(), //SENÃO não é obrigatorio
        }),

        password_confirmation: Yup.string().when('old_password', {
          is: (val: string) => !!val.length, //Verifica se 'old_password' possui valor
          then: Yup.string().required('Campo obrigatório!'), //Se 'old_password' possuir valor, 'password' é Obrigatório
          otherwise: Yup.string(), //SENÃO não é obrigatorio
        })
        .oneOf([Yup.ref('password'), null], 'Confirmação Incorreta!'), //Verifica se foi informado a mesma senha
      });

      await schema.validate(data, {
        abortEarly: false, /*Para retornar TODOS os ERROS de uma única vez*/
      });

      const { name, email, password, old_password, password_confirmation } = data;

      //Gera um novo Objeto apartir de DOIS OBJETOS, quando for informado o old_password
      //na requisição será enviado os campos name, email, password, old_password, password_confirmation,
      //caso contrário será enviado apenas o name e o email
      const formData = {
        name,
        email,
        ...(old_password
          ? {
            password,
            old_password,
            password_confirmation
          } :
            {})
      }

      //Envia para a API os dados do usuário para que possa ser atualizado o Perfil
      const response = await api.put('/profile', formData);

      //Atualiza os dados do usuário logado
      updateUser(response.data); 

      Alert.alert('Perfil atualizado com sucesso')
      navigation.goBack(); //Volta para a tela de Login
      
    } catch (err) {
      if (err instanceof Yup.ValidationError){   //Verifica se o Erro ocorreu na validação utilizando o schema do Yup
        const errors = getValidationErrors(err); //Trata os ERROS e carrega as mensagens nos Inputs
        formRef.current?.setErrors(errors);
      }
      Alert.alert(
        'Erro na atualização do perfil', 
        'Ocorreu um erro ao atualizar o seu perfil, tente novamente.'
      );      
    }
  }, []);


  const handleGoBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);
  
  return (
    <>
    <KeyboardAvoidingView 
      //Controla para que o teclado não se sobreponha sobre a tela
      //quando o sistema for Ios
      style={{ flex: 1 }}
      behavior={Platform.OS == 'ios' ? 'padding' : undefined}
      enabled
    >
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ flex: 1 }}
          nestedScrollEnabled={true}
        >
          <Container>
            <BackButton onPress={handleGoBack}>
              <Icon name="chevron-left" size={24} color="#999591" />
            </BackButton>

            <UserAvatarButton onPress={() => {}}>
              <UserAvatar source={{ uri: user.avatar_url }} />
            </UserAvatarButton>

            <View>
              <Title>Meu Perfil</Title>
            </View>

            <Form initialData={{name: user.name, email: user.email}} ref={ formRef } onSubmit={ handleSignUp } style={{"width":"100%"}} > 
              <Input 
                autoCapitalize="words"
                name="name" 
                icon="user" 
                placeholder="Nome" 
                returnKeyType="next"
                onSubmitEditing={() => {
                  email.current?.focus();
                }}                
              />
              <Input 
                ref={email}
                keyboardType="email-address"
                autoCorrect={false}
                autoCapitalize="none"
                name="email" 
                icon="mail" 
                placeholder="E-Mail" 
                returnKeyType="next"
                onSubmitEditing={() => {
                  oldPassword.current?.focus();
                }}                
              />
              <Input 
                ref={oldPassword}
                secureTextEntry
                name="old_password" 
                icon="lock" 
                placeholder="Senha atual"
                textContentType="newPassword" 
                returnKeyType="next"
                //containerStyle={{ marginTop: 16 }}
                onSubmitEditing={() => {
                  newPassword.current?.focus();	
                }}                                                 
              />

              <Input 
                ref={newPassword}
                secureTextEntry
                name="password" 
                icon="lock" 
                placeholder="Nova Senha"
                textContentType="newPassword" 
                returnKeyType="next"
                onSubmitEditing={() => {
                  confirmationPassword.current?.focus();	
                }}                                                 
              />              

              <Input 
                ref={confirmationPassword}
                secureTextEntry
                name="password_confirmation" 
                icon="lock" 
                placeholder="Confirmar Senha"
                textContentType="newPassword" 
                returnKeyType="send"
                onSubmitEditing={() => {
                  formRef.current?.submitForm();	
                }}                                                 
              />                            

              <Button onPress={() => formRef.current?.submitForm() }>
                Confirmar Mudanças
              </Button>    
            </Form>
          </Container>
        </ScrollView>   
      </KeyboardAvoidingView>   
    </>
  );
}

export default Profile;