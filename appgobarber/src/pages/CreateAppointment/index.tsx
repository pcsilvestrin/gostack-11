import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Alert, Platform } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/core';
import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import { format } from 'date-fns';
import DateTimePicker from '@react-native-community/datetimepicker';

import { useAuth } from '../../context/AuthContext';
import api from '../../services/api';

import { 
  Container,
  Header,
  BackButton,
  HeaderTitle,
  UserAvatar,

  Content,
  ProviderListContainer,
  ProviderContainer,
  ProviderAvatar,
  ProviderName,
  Calendar,
  Title,
  OpenDatePickerButton,
  OpenDatePickerButtonText,

  Schedule,
  Section,
  SectionTitle,
  //SectionContent,
  Hour,
  HourText,

  CreateAppointmentButton,
  CreateAppointmentButtonText
} from './styles';

interface IRouteParams {
  providerId: string;
}

export interface IProvider {
  id: string;
  name: string;
  avatar_url: string;
}

interface IAvailabilityItem {
  hour: number;
  available: boolean;
}

const CreateAppointment: React.FC = () => {
  const { user } = useAuth();
  const route = useRoute();
  const { goBack, navigate } = useNavigation();

  //Tipa os parametros passados na Rota
  //Onde é esperado que o route.params seja um IRouteParams
  const routeParams = route.params as IRouteParams; 

  const [availability, setAvailability] = useState<IAvailabilityItem[]>([]);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [selectedDate, setSelectedDate] = useState(new Date()); //Armazena a data selecionada  
  const [selectedHour, setSelectedHour ] = useState(0); //Armazena o Horário selecionado para agendamento
  const [providers, setProviders] = useState<IProvider[]>([]);

  //Para armazenar o Provider selecionado
  const [selectedProvider, setSelectedProvider] = useState(routeParams.providerId);

  useEffect(() => {  
    api.get('providers').then((response) => {
      setProviders(response.data);
    })
  }, [setProviders]);

  useEffect(() => {
    //Carrega os Horários do Dia selecionado por Provider
    api.get(`providers/${selectedProvider}/day-availability`, {
      params: {
        year: selectedDate.getFullYear(),
        month: selectedDate.getMonth() + 1,
        day: selectedDate.getDate(),
      },
    }).then((response) => {
      setAvailability(response.data);
    })
  }, [selectedDate, selectedProvider]);

  const navigateBack = useCallback(() => {
    goBack();
  }, [goBack]);

  const handleSelectProvider = useCallback((providerId: string) => {
    setSelectedProvider(providerId);
  }, []);

  const handleToggleDatePicker = useCallback(() => {
    setShowDatePicker(!showDatePicker);
  }, []);

  const handleDateChange = useCallback((event: any, date: Date | undefined) => {
    if (Platform.OS === 'android') {
      setShowDatePicker(false);
    }

    //Salva a data selecionada
    if (date) {
      setSelectedDate(date);
    }
  }, []);

  const handleSelectHour = useCallback((hour: number) => {
    setSelectedHour(hour);
  }, []);

  const handleCreateAppointment = useCallback( async () => {
    try {
      const date = new Date(selectedDate);

      date.setHours(selectedHour);
      date.setMinutes(0);

      await api.post('appointments', {
        provider_id: selectedProvider,
        date,
      });

      navigate('AppointmentCreated', { date: date.getTime() });
    } catch (error) {
      Alert.alert(
        'Erro ao criar agendamento',
        'Ocorreu um erro ao tentar criar o agendamento, tente novamente!',
      )
    }
  }, [navigate, selectedDate, selectedHour, selectedProvider]);

  const morningAvailability = useMemo(() => {
    return availability
      .filter(({ hour }) => hour < 12)
      .map(({ hour, available }) => {
        return {
          hour,
          available,
          hourFormatted: format(new Date().setHours(hour), 'HH:00'),
        }
      })
  }, [availability]);

  const afternoonAvailability = useMemo(() => {
    return availability
      .filter(({ hour }) => hour >= 12)
      .map(({ hour, available }) => {
        return {
          hour,
          available,
          hourFormatted: format(new Date().setHours(hour), 'HH:00'),
        }
      });
  }, [availability]);  
 
  return (
    <Container>
      <Header>
        <BackButton onPress={navigateBack}>
          <Icon name="chevron-left" size={24} color="#999591" />
        </BackButton> 

        <HeaderTitle>Cabeleireiros</HeaderTitle>
        <UserAvatar source={{ uri: user.avatar_url }} />
      </Header>

      <Content>
        <ProviderListContainer>
          <FlatList 
            horizontal /*A lista será gerada na Horizontal*/
            showsHorizontalScrollIndicator={false} /*Esconde a bara de navegação*/
            data={providers}
            keyExtractor={(provider) => provider.id}
            renderItem={({ item: provider }) => (
              <ProviderContainer
                //Passa para o styled Component se o Provider da lista é o selecionado
                selected={provider.id === selectedProvider}
                onPress={() => handleSelectProvider(provider.id)} //Seta o ID do Provider selecionado
              >
                <ProviderAvatar source={{ uri: provider.avatar_url }} />
                <ProviderName 
                  selected={provider.id === selectedProvider}
                >{provider.name}</ProviderName>
              </ProviderContainer>    
            )} 
          />         
        </ProviderListContainer>

        <Calendar>
          <Title>Escolha a Data</Title>    

          <OpenDatePickerButton onPress={handleToggleDatePicker}>
            <OpenDatePickerButtonText>Selecionar outra data</OpenDatePickerButtonText>
          </OpenDatePickerButton>

          {showDatePicker && (
            <DateTimePicker
              {...(Platform.OS === 'ios' && { textColor: '#f4ede8' })} // < nessa linha
              mode="date" /*Mostra apenas a Data*/
              onChange={handleDateChange} /*Armazena a nova data selecionada*/
              display={Platform.OS === 'android' ? 'calendar' : 'spinner'}
              value={selectedDate}
            />
          )}    
        </Calendar>      

        <Schedule>
            <Title>Escolha o Horário</Title>
            <Section>
              <SectionTitle>Manhã</SectionTitle>

              <FlatList 
                horizontal /*A lista será gerada na Horizontal*/
                showsHorizontalScrollIndicator={false} /*Esconde a bara de navegação*/
                data={morningAvailability}
                keyExtractor={(item) => item.hourFormatted}
                renderItem={({ item: hourMorning }) => (
                  <Hour 
                    enabled={hourMorning.available}
                    selected={selectedHour === hourMorning.hour} //Verifica se o Horário listado é o Selecionado
                    available={hourMorning.available} 
                    key={hourMorning.hourFormatted}
                    onPress={() => handleSelectHour(hourMorning.hour)}
                  >
                    <HourText selected={selectedHour === hourMorning.hour}>{hourMorning.hourFormatted}</HourText>
                  </Hour>
                )} 
              />                       
            </Section>

            <Section>
              <SectionTitle>Tarde</SectionTitle>

              <FlatList 
                horizontal /*A lista será gerada na Horizontal*/
                showsHorizontalScrollIndicator={false} /*Esconde a bara de navegação*/
                data={afternoonAvailability}
                keyExtractor={(item) => item.hourFormatted}
                renderItem={({ item: hourAfterNonn }) => (
                  <Hour 
                    enabled={hourAfterNonn.available}
                    selected={selectedHour === hourAfterNonn.hour} //Verifica se o Horário listado é o Selecionado
                    available={hourAfterNonn.available} 
                    key={hourAfterNonn.hourFormatted}
                    onPress={() => handleSelectHour(hourAfterNonn.hour)}
                  >
                    <HourText selected={selectedHour === hourAfterNonn.hour}>{hourAfterNonn.hourFormatted}</HourText>
                  </Hour>
                )} 
              />                                     
            </Section>          
        </Schedule>    

        <CreateAppointmentButton onPress={handleCreateAppointment}>
          <CreateAppointmentButtonText>Agendar</CreateAppointmentButtonText>
        </CreateAppointmentButton>
      </Content>
    </Container>
  )
};

export default CreateAppointment;