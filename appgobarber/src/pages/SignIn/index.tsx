import React, { useCallback, useRef } from 'react';
import { Image, 
         View, 
         ScrollView, 
         KeyboardAvoidingView, 
         Platform, 
         TextInput,
         Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';
import * as Yup from 'yup';

import { Form } from '@unform/mobile';
import { FormHandles } from '@unform/core';

import { useAuth } from '../../context/AuthContext';

import Input from '../../components/Input'; 
import Button from '../../components/Button';

import logoImg from '../../assets/logo.png';
import getValidationErrors from '../../utils/getValidationErrors';

import { Container, 
         Title, 
         ForgotPassword, 
         ForgotPasswordText, 
         CreateAccountButton, 
         CreateAccountButtonText 
} from './styles';

interface SignInFormData {
  email: string;
  password: string;
}

const SignIn: React.FC = () => {
  //FormHandles - Possibilita manipular as propriedades do Form
  const formRef = useRef<FormHandles>(null);
  const passwordInputRef = useRef<TextInput>(null); //Para setar o foco no campo de Senha
  const navigation = useNavigation();

  const { signIn, user } = useAuth();
  console.log(user);

  const handleSignIn = useCallback(async (data: SignInFormData) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        email: Yup.string().required('Email obrigatório').email('Digite um e-mail válido'),
        password: Yup.string().required('Senha obrigatória')
      });

      await schema.validate(data, {
        abortEarly: false, /*Para retornar TODOS os ERROS de uma única vez*/
      });

      await signIn({
        email: data.email,
        password: data.password
      });

    } catch (err) {
      //Verifica se o Erro ocorreu na validação utilizando o schema do Yup
      if (err instanceof Yup.ValidationError){
        //Trata os ERROS e carrega as mensagens nos Inputs
        const errors = getValidationErrors(err);
        formRef.current?.setErrors(errors);

        //return;
      }

      Alert.alert('Erro na autenticação', 'Ocorreu um erro ao fazer o login, cheque as credênciais.')
    }
  }, []);

  return (
    <>
      <KeyboardAvoidingView 
        //Controla para que o teclado não se sobreponha sobre a tela
        //quando o sistema for Ios
        style={{ flex: 1 }}
        behavior={Platform.OS == 'ios' ? 'padding' : undefined}
        enabled
      >
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ flex: 1 }}
        >
          <Container>
            <Image source={logoImg} />
            <View>
              <Title>Faça seu Logon</Title>
            </View>

            <Form ref={ formRef } onSubmit={ handleSignIn } style={{"width":"100%"}} > 
              <Input 
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="email-address"
                name="email" 
                icon="mail" 
                placeholder="E-Mail" 
                returnKeyType="next"
                onSubmitEditing={() => {
                  passwordInputRef.current?.focus();
                }}
              />
              <Input 
                ref={passwordInputRef}
                name="password" 
                icon="lock" 
                placeholder="Senha" 
                secureTextEntry
                textContentType="newPassword" 
                returnKeyType="send"
                onSubmitEditing={() => {
                  formRef.current?.submitForm();	
                }}                                 
              />

              <Button
                onPress={() => {
                  //Executa o Evento Submit do Formulário
                  formRef.current?.submitForm();
                }}
              >Entrar</Button>    
            </Form>

            <ForgotPassword onPress={() => {}}>
              <View>
                <ForgotPasswordText>Esqueci minha senha</ForgotPasswordText>  
              </View>  
            </ForgotPassword>  
          </Container>
        </ScrollView>        
        <CreateAccountButton onPress={() => navigation.navigate('SignUp')}>
          <Icon name="log-in" size={20} color="#ff9000" />
          <CreateAccountButtonText>Criar uma conta</CreateAccountButtonText>
        </CreateAccountButton>
      </KeyboardAvoidingView>
    </>
  );
}

export default SignIn;