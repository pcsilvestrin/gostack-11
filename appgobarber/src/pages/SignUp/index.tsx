import React, { useRef, useCallback } from 'react';
import { Image, 
         View, 
         ScrollView, 
         KeyboardAvoidingView, 
         Platform,
         TextInput,
         Alert 
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';

import { Form } from '@unform/mobile';
import { FormHandles } from '@unform/core';

import * as Yup from 'yup';
import getValidationErrors from '../../utils/getValidationErrors';

import api from '../../services/api';
import Input from '../../components/Input'; 
import Button from '../../components/Button';

import logoImg from '../../assets/logo.png';

import { Container, 
         Title, 
         BackToSignIn, 
         BackToSignInText 
} from './styles';

const SignUp: React.FC = () => {
  //FormHandles - Possibilita manipular as propriedades do Form
  const formRef = useRef<FormHandles>(null);
  const navigation = useNavigation();

  const emailInputRef = useRef<TextInput>(null);
  const passwordInputRef = useRef<TextInput>(null);  

  interface SignUpFormData {
    name: string;
    email: string;
    password: string;
  }

  const handleSignUp = useCallback(async (data: SignUpFormData) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        name: Yup.string().required('Nome obrigatório'),
        email: Yup.string().required('Email obrigatório').email('Digite um e-mail válido'),
        password: Yup.string().min(6, 'No mínimo 6 digitos'),
      });

      await schema.validate(data, {
        abortEarly: false, /*Para retornar TODOS os ERROS de uma única vez*/
      });

      //Envia para a API os dados do usuário para que possa ser salvo
      await api.post('/users', data);
      Alert.alert('Cadastro realizado com sucesso', 'Você já pode fazer login na aplicação.')
      navigation.goBack(); //Volta para a tela de Login
      
    } catch (err) {
      if (err instanceof Yup.ValidationError){   //Verifica se o Erro ocorreu na validação utilizando o schema do Yup
        const errors = getValidationErrors(err); //Trata os ERROS e carrega as mensagens nos Inputs
        formRef.current?.setErrors(errors);
      }
      console.log(err);
      Alert.alert('Erro no Cadastro', 'Ocorreu um erro ao realizar o cadastro do usuário.');      
    }
  }, []);

  return (
    <>
      <KeyboardAvoidingView 
        //Controla para que o teclado não se sobreponha sobre a tela
        //quando o sistema for Ios
        style={{ flex: 1 }}
        behavior={Platform.OS == 'ios' ? 'padding' : undefined}
        enabled
      >
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ flex: 1 }}
        >
          <Container>
            <Image source={logoImg} />
            <View>
              <Title>Crie sua conta</Title>
            </View>

            <Form ref={ formRef } onSubmit={ handleSignUp } style={{"width":"100%"}} > 
              <Input 
                autoCapitalize="words"
                name="name" 
                icon="user" 
                placeholder="Nome" 
                returnKeyType="next"
                onSubmitEditing={() => {
                  emailInputRef.current?.focus();
                }}                
              />
              <Input 
                ref={emailInputRef}
                keyboardType="email-address"
                autoCorrect={false}
                autoCapitalize="none"
                name="email" 
                icon="mail" 
                placeholder="E-Mail" 
                returnKeyType="next"
                onSubmitEditing={() => {
                  passwordInputRef.current?.focus();
                }}                
              />
              <Input 
                ref={passwordInputRef}
                secureTextEntry
                name="password" 
                icon="lock" 
                placeholder="Senha"
                textContentType="newPassword" 
                returnKeyType="send"
                onSubmitEditing={() => {
                  formRef.current?.submitForm();	
                }}                                                 
              />

              <Button 
                onPress={() => formRef.current?.submitForm() }
              >Entrar</Button>    
            </Form>
          </Container>
        </ScrollView>        

        <BackToSignIn onPress={() => navigation.goBack()}>
          <Icon name="arrow-left" size={20} color="#fff" />
          <BackToSignInText>Voltar para Logon</BackToSignInText>
        </BackToSignIn>
      </KeyboardAvoidingView>
    </>
  );
}

export default SignUp;