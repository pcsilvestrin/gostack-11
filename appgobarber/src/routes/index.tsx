import React from 'react';
import { View, ActivityIndicator } from 'react-native';

import AuthRoutes from './auth.routes';
import AppRoutes from './app.routes';

import { useAuth } from '../context/AuthContext';

const Routes: React.FC = () => {
  const { user, loading } = useAuth();

  //Enquanto estiver carregando os dados de Login, apresenta uma tela com o Spinner de Aguarde 
  //para que não pisque na tela do celular a tela de Login
  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color="#999" />
      </View>
    )
  }

  //SE o usuário estiver logado, abre direto as rotas da Aplicação, como a Dashboard
  //SENÃO abre as rotas que não precisa estar logado como a SignIn e SignUp
  return  user ? <AppRoutes /> : <AuthRoutes />;
} 

export default Routes;