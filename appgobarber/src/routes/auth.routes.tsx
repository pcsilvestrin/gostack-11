import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';

const Auth = createStackNavigator();

const AuthRoutes: React.FC = () => (
  <Auth.Navigator
    screenOptions={{ /* Configurações do componente de , pode ser configurado o Header da Aplicação */
      headerShown: false, /* Remove o Titulo da Página */
      cardStyle: { backgroundColor: '#312e38' },
    }}
    //initialRouteName="SignUp" //Define a página Inicial 
  >
    <Auth.Screen name="SignIn" component={SignIn} />
    <Auth.Screen name="SignUp" component={SignUp} />    
  </Auth.Navigator>
);

export default AuthRoutes;