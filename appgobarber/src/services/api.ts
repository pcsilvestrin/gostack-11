import axios from 'axios';

const api = axios.create({
  //Endereço da máquina onde a API está Rodando
  baseURL: 'http://192.168.1.13:3333', 
});

export default api;