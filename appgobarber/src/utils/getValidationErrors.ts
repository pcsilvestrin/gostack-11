import { ValidationError } from 'yup';

//Interface genérica, para tratar os erros que qualquer Form
interface Errors {
  //key - Será o nome do input que gerou o erro e o seu valor será a mensagem de erro
  [key: string]: string;
}

export default function getValidationErrors(err: ValidationError): Errors{
  const validationErrors: Errors = {};

  //Percorre os erros do Form
  err.inner.forEach((error) => {
    validationErrors[error.path!] = error.message;
  });

  console.log(validationErrors);

  return validationErrors;
}
