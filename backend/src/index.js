const express = require('express');
const cors = require('cors');
const { v4: uuidv4 } = require('uuid');
const { isUuid } = require('uuidv4');

const app = express();

app.use(cors());
app.use(express.json());

const projects = [];

function logRequests(request, response, next){
   //Captura o nome do método (GET, POST, PUT, DELETE) e a url, obtidos do request
   const { method, url } = request;

   //Gera uma leganda com as informações obtidas acima
   const logLabel = `[${method.toUpperCase()}] ${url}`;
   console.log(logLabel);

   //O next é utilizado para que conitune a execução, no caso
   //será executado o método original que foi chamado
   return next();
}

function validateProjectId(request, response, next){
   const { id } = request.params;

   if (!isUuid(id)){
      return response.status(400).json({ error: 'Invalid project ID'});
   }
   return next();
}

//app.use - Utilizado para definir o que deve ser executado antes
//das funções principais
app.use(logRequests);

//Para todas as rotas de '/projects/:id', será executado o Middleware 'validateProjectId',
//desta forma, é possível anexar diversos Middleares
app.use('/projects/:id', validateProjectId);

//Consulta
app.get('/projects', (request, response) => {
   const { title } = request.query;

   const results = title
       ? projects.filter(project => project.title.includes(title))
       : projects;

   console.log(title);
   console.log(projects);
   return response.json(results);
});

//Inclusão
app.post('/projects', (request, response) => {
   const { title, owner } = request.body;
   const project = {
      id: uuidv4(),
      title,
      owner
   };
   projects.push(project);

   return response.json(project);
});

//Edição
app.put('/projects/:id', (request, response) => {
   const { id } = request.params;
   const { title, owner } = request.body;

   //Encontra o indice do elemento dentro do Array
   const projectIndex = projects.findIndex(project => project.id === id);

   if (projectIndex < 0){
      return response.status(400).json({ error: 'Project not found.' });
   }

   const project = {
      id,
      title,
      owner
   };

   //Atualiza o objeto no Array, de acordo com o Indice
   projects[ projectIndex ] = project;

   return response.json(project);
});

app.delete('/projects/:id', (request, response) => {
   const { id } = request.params;
   const projectIndex = projects.findIndex(project => project.id === id);

   if (projectIndex < 0){
      return response.status(400).json({ error: 'Project not found.' });
   }

   projects.splice(projectIndex, 1);
   return response.status(204).json(projects);
});

//Configura a porta onde o projeto será executado
app.listen(3333, () => {
   console.log('o(*￣▽￣*)ブ Server Rodando!');
});
