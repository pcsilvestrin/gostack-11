import axios from 'axios'; 

const api = axios.create({
  baseURL: 'http://192.168.1.14:3333',
});

/** COMO DEFINIR A URL:
 * iOS com Emulador: localhost
 * iOS com Fisico: IP da máquina
 * Android com Emulador: localhost (adb reverse)
 * Android com Emulador: 10.0.2.2 (Android Studio)
 * Andoid com Emulador: 10.0.3.2 (Genymotion)
 * Android com Físico: IP da Máquina
*/

export default api;