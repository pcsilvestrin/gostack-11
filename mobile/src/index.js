import React, { useEffect, useState } from 'react';
import { SafeAreaView, FlatList, Text, StyleSheet, StatusBar, TouchableOpacity } from 'react-native';

import api from './services/api';

export default function App(){
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    api.get('projects').then(response => {
      console.log(response.data);
      setProjects(response.data);
    });
  }, []);

  async function handleAddProject(){
    const response = await api.post('projects', {
      title: `Novo Projeto ${Date.now()}`,
      owner: 'Paulo Silvestrin'
    });

    const project = response.data;
    setProjects([...projects, project]);
  }

  /*Define que a 'View' vai utilizar a estilização configurada para o container*/
  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="#7159C1"/>

      <SafeAreaView style={styles.container}>
        <FlatList /*Componente para apresentar listas*/
          data={projects}          /*Array de Informações, será percorrido e gerado na tela os elementos*/
          keyExtractor={project => project.id} /*Campo chave do Objeto*/
          renderItem={({ item: project }) => ( /*Renderiza na tela os elementos do Array*/
            <Text style={styles.project}>{project.title}</Text>
          )}
        />
      </SafeAreaView> 

      <TouchableOpacity 
        activeOpacity={0.6}     /*Grau de Opacidade - de 0 a 9*/
        style={styles.button}   /*Define o Estilo que será aplicado no "Botão"*/
        onPress={handleAddProject} /*Evento que será executado ao pressionar o Elemento*/
      >
         <Text style={styles.buttonText}>Adicionar Projeto</Text>      
      </TouchableOpacity>      

    </>  
  );
}

/*Cria os estilos css para os componentes*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7159C1',
  },
  project: {
    color: '#FFF',
    fontSize: 20,
  },
  button: {
    backgroundColor: '#FFF',
    margin: 20,
    height: 30,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },  
  buttonText: {
    fontWeight: 'bold',
    fontSize: 16,
  },    

});