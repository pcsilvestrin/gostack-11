const path = require('path');

module.exports = {
  /*Define o arquivo de entrada, o primeiro que será executado na aplicação*/  
  entry: path.resolve(__dirname, 'src', 'index.js'), 

  /*Define qual arquivo será gerado após ser convertido, neste caso é o "bundle.js"*/
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  devServer: {
    /*Define os arquivos publicos para que sejam Monitorados*/
    contentBase: path.resolve(__dirname, 'public'),
  },
  module: {
    rules: [ 
      {
        test: /\.js$/, /*Pega todos os arquivos com a extenção JS para serem convertidos pelo Babel*/
        exclude: /node_modules/, /*Desconsidera os arquivos JS do node_modules*/
        use: {
          loader: 'babel-loader', /*Arquivos JS serão convertidos pelo 'babel-loader'*/
        }
      },
      {
        test: /\.css$/, /*Pega todos os arquivos de CSS*/
        exclude: /node_modules/, /*Desconsidera os arquivos desta pasta*/
        use : [
          { loader: 'style-loader' }, /*Captura o css interpretado pelo css-loader e injeta no html*/
          { loader: 'css-loader' },   /*Realiza a leitura e interpretações dos arquivos CSS*/
        ]
      },
      {
        test: /.*\.(gif|png|jpe?g)$/i, /*Pega todos os arquivos de imagem, respeitando a extenção*/
        use: {
          loader: 'file-loader' 
        } 
      }
    ]
  }
};