module.exports = {
  presets: [ /*Declara os arquivos de terceiros que serão utilizados*/
    '@babel/preset-env',  /*Converte o código baseado no ambiente em que está sendo executado*/
    '@babel/preset-react' /*Converte o html 'embutido' no JS, de uma forma que o Browser também entenda*/ 
  ],
  plugins: [
    '@babel/plugin-transform-runtime'
  ]
};