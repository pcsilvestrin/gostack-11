import React, { useState, useEffect } from 'react';
import Header from './components/Header';
import api from './services/api';

import './App.css';

export default function App(){
  const [projects, setProjects] = useState([]);

  //Dois Parâmetros
  //1º Qual função será disparada
  //2º Quando a função deve ser disparada
  useEffect(() => {
    api.get('/projects').then(response => {
      setProjects(response.data);
    });
  }, []);

  async function handleAddProject(){
    
    const response = await api.post('/projects', {
      title : `Novo elemento ${Date.now()}`,
      owner : "Paulo"
    });

    const project = response.data;
    setProjects([...projects, project]); //IMUTABILIDADE   
  }

  return (
    <>
      <Header title="Projects" />      
      <ul>
        {projects.map(project => <li key={project.id}>{project.title}</li>)}
      </ul>

      <button type="button" onClick={handleAddProject}>Adicionar Projeto</button>
    </>
  );
}
