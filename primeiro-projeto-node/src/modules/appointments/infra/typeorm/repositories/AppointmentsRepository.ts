import Appointment from '../entities/Appointment';
import { getRepository, Raw, Repository } from 'typeorm';

import IAppointmentsRepository from '@modules/appointments/repositories/IAppointmentsRepository';
import ICreateAppointmentDTO from '@modules/appointments/dtos/ICreateAppointmentDTO';
import IFindAllMonthFromProviderDTO from '@modules/appointments/dtos/IFindAllMonthFromProviderDTO';
import IFindAllDayFromProviderDTO from '@modules/appointments/dtos/IFindAllDayFromProviderDTO';

class AppointmentRespository implements IAppointmentsRepository {
  private ormRepository: Repository<Appointment>;

  constructor(){
    this.ormRepository = getRepository(Appointment);
  }

  public async findByDate(date: Date, provider_id: string): Promise<Appointment | undefined>  {
    //Faz uma busca, filtrando pela Data
    const findAppointment = await this.ormRepository.findOne({
      where: { date, provider_id },
    });
    return findAppointment || undefined;
  }

  public async create({ provider_id, user_id, date }: ICreateAppointmentDTO): Promise<Appointment>{
    //Cria o objeto Appointment
    const appointment = this.ormRepository.create({ provider_id, user_id, date });

    //Salva o objeto no Banco
    await this.ormRepository.save(appointment);

    return appointment;
  }

  public async findAllInMonthFromProvider({ provider_id, month, year }: IFindAllMonthFromProviderDTO): Promise<Appointment[]>{
    //Quando o mês não possuir dois Digitos, é adicionado o Zero a esquerda
    const parseMonth = String(month).padStart(2, '0');

    const appointments = await this.ormRepository.find({
      where: {
        provider_id,
        date: Raw(dateFieldName =>  //Raw - Executa o código sem que seja alterado pelo TYPEORM
          `to_char(${dateFieldName}, 'MM-YYYY') = '${parseMonth}-${year}'`, //Converte um valor para string, já formatando
        ),
      }
    });

    return appointments;
  }

  public async findAllDayFromProvider({ provider_id, day, month, year }: IFindAllDayFromProviderDTO): Promise<Appointment[]>{
    //Quando o mês não possuir dois Digitos, é adicionado o Zero a esquerda
    const parseMonth = String(month).padStart(2, '0');
    const parseDay   = String(day).padStart(2, '0');

    const appointments = await this.ormRepository.find({
      where: {
        provider_id,
        date: Raw(dateFieldName =>  //Raw - Executa o código sem que seja alterado pelo TYPEORM
          `to_char(${dateFieldName}, 'DD-MM-YYYY') = '${parseDay}-${parseMonth}-${year}'`, //Converte um valor para string, já formatando
        ),
      },
      relations: ['user'], //Retorna o Cliente a quem pertence o Agendamento
    });

    return appointments;
  }
}

export default AppointmentRespository;
