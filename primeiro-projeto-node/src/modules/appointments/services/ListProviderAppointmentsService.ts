import { injectable, inject } from 'tsyringe'; //Injeção de Dependência

import Appointment from '../infra/typeorm/entities/Appointment';
import IAppointmentsRepository from '../repositories/IAppointmentsRepository';
import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';
import { classToClass } from 'class-transformer';

interface IRequest{
  provider_id: string;
  day   : number;
  month : number;
  year  : number;
}

@injectable()
class ListProviderAppointmentsService {
  constructor(
     @inject('AppointmentsRepository')
     private appointmentRepository: IAppointmentsRepository,

     @inject('CacheProvider')
     private cacheProvider: ICacheProvider,
  ){}

  public async execute({ provider_id, day, year, month }: IRequest): Promise<Appointment[]>{
    //Cria a chave de Agendamentos por Provider, Ano, Mês e Dia
    const cacheKey = `provider-appointments:${provider_id}:${year}-${month}-${day}`;

    //Recarrega os appointments salvos no Redis, utilizando a Chave criada acima
    let appointments = await this.cacheProvider.recover<Appointment[]>(cacheKey);

    if (!appointments) {
      //Carrega os Agendamentos por Usuário, Dia, Mes e Ano
      appointments = await this.appointmentRepository.findAllDayFromProvider({
        provider_id,
        day,
        year,
        month
      });

      //console.log('Buscou do Banco');

      //Atualiza os Appointments no Redis
      await this.cacheProvider.save(cacheKey, classToClass(appointments));
    }

    return appointments;
  }
}

export default ListProviderAppointmentsService;

