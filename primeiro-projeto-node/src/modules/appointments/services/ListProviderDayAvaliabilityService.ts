import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import { getHours, isAfter } from 'date-fns';
import IAppointmentsRepository from '../repositories/IAppointmentsRepository';

interface IRequest{
  provider_id: string;
  day   : number;
  month : number;
  year  : number;
}

type IResponse = Array<{
  hour: number;
  available: boolean; //Disponivel
}>;

@injectable()
class ListProviderDayAvaliabilityService {
  constructor(
     @inject('AppointmentsRepository')
    private appointmentRepository: IAppointmentsRepository,
  ){}

  public async execute({ provider_id, day, year, month }: IRequest): Promise<IResponse>{
    //Carrega os agendamentos do Dia
    const appointments = await this.appointmentRepository.findAllDayFromProvider({
      provider_id,
      day,
      year,
      month
    });

    const hourstart = 8; //Só pode ter agendamento apartir das 8

    //Gera o seguinte Array [8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
    const eachHourArray = Array.from(
      { length: 10 }, //Array com 10 posições, máximo de 10 agendamentos por dia
      (_, index) => index + hourstart,
    );

    const currentDate = new Date(Date.now());

    const availability = eachHourArray.map(hour => {

      //Verifica se no Dia existe agendamento para o Horário
      const hasAppointmentHour = appointments.find(
        appointment => getHours(appointment.date) === hour,
      );

      const compareDate = new Date(year, month-1, day, hour);

      return {
        hour,
        available: !hasAppointmentHour && isAfter(compareDate, currentDate),
      }
    });

    return availability;
  }
}

export default ListProviderDayAvaliabilityService;

