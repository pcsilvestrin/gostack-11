import FakeCacheProvider from '@shared/container/providers/CacheProvider/fakes/FakeCacheProvider';
import FakeAppointmentRepository from '../repositories/fakes/FakeAppointmentsRepository';
import ListProviderAppointmentsService from './ListProviderAppointmentsService';

let fakeCachProvider: FakeCacheProvider;
let fakeAppointmentRepository : FakeAppointmentRepository;
let listProviderAppointmentService: ListProviderAppointmentsService;

describe('ListProviderAppointmentsService', () => {
  beforeEach(() => {
    fakeCachProvider = new FakeCacheProvider();
    fakeAppointmentRepository = new FakeAppointmentRepository();

    listProviderAppointmentService = new ListProviderAppointmentsService(
      fakeAppointmentRepository,
      fakeCachProvider
    );
  });

  it('Listar os Agendamentos por Barbeiro x Dia x Mes x Ano', async () => {

    const appointment1 = await fakeAppointmentRepository.create({
      provider_id: 'provider',
      user_id: 'user',
      date: new Date(2020, 4, 20, 14, 0, 0),
    });

    const appointment2 = await fakeAppointmentRepository.create({
      provider_id: 'provider',
      user_id: 'user',
      date: new Date(2020, 4, 20, 15, 0, 0),
    });

    const appointments = await listProviderAppointmentService.execute({
      provider_id: 'provider',
      year: 2020,
      month: 5,
      day: 20,
    });

    //Espera que seja retornado um Array com os dois Agendamentos criados
    expect(appointments).toEqual([
      appointment1,
      appointment2
    ]);
  });
});
