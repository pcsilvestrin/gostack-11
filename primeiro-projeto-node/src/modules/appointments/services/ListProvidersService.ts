import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import { classToClass } from 'class-transformer';

import User from '@modules/users/infra/typeorm/entities/User';
import IUserRepository from '@modules/users/repositories/IUserRepository';
import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';

interface IRequest{
  user_id: string;
}

@injectable()
class ListProvidersService {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUserRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider
  ){}

  public async execute({ user_id }: IRequest): Promise<User[]>{

    //Carrega a lista de usuários salva no Redis
    let users = await this.cacheProvider.recover<User[]>(
      `providers-list:${user_id}`,
    );

    //Se não possuir dados no Redis, é realizado a consulta no
    //banco PostGres e atualizados os registros no Redis.
    if (!users) {
      //Realizar a busca no PostGres
      users = await this.usersRepository.findAllProviders({
        except_user_id: user_id,
      });

      //console.log('Consultou PostGres!');

      //Salva no Redis
      await this.cacheProvider.save(
        `providers-list:${user_id}`,
        classToClass(users)
      );
    }

    return users;
  }
}

export default ListProvidersService;

