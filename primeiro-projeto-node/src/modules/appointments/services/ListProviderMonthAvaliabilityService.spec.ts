import FakeAppointmentRepository from '../repositories/fakes/FakeAppointmentsRepository';
import ListProviderMonthAvaliabilityService from './ListProviderMonthAvaliabilityService';

let fakeAppointmentRepository : FakeAppointmentRepository;
let listProviderMonthAvaliabilityService: ListProviderMonthAvaliabilityService;

describe('ListProviderMonthAvaliabilityService', () => {
  beforeEach(() => {
    fakeAppointmentRepository = new FakeAppointmentRepository();
    listProviderMonthAvaliabilityService = new ListProviderMonthAvaliabilityService(fakeAppointmentRepository);
  });

  it('Listar dias onde existe Horários disponiveis para agendamento (por Barbeiro)', async () => {
    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 3, 20, 8, 0, 0), // 20-04-2020 08:00:00
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 8, 0, 0), // 20-04-2020 08:00:00
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 9, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 10, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 11, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 12, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 13, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 14, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 15, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 16, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 20, 17, 0, 0),
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2025, 4, 21, 8, 0, 0), // 21-04-2020 08:00:00
    });

    //Consulta os agendamentos do usuário fake '122' no mês 05/2020
    const availability = await listProviderMonthAvaliabilityService.execute({
      provider_id: '121',
      year: 2025,
      month: 5,
    });

    //console.log(availability);

    //Espera que o retorno seja um array e
    //que os dias 20 e 21 estejam com available: false
    expect(availability).toEqual(
      expect.arrayContaining([
        { day: 19, available: true },
        { day: 20, available: false },
        { day: 21, available: true },
        { day: 22, available: true },
      ])
    );

  });
});
