import FakeAppointmentRepository from '../repositories/fakes/FakeAppointmentsRepository';
import ListProviderDayAvaliabilityService from './ListProviderDayAvaliabilityService';

let fakeAppointmentRepository : FakeAppointmentRepository;
let listProviderDayAvaliabilityService: ListProviderDayAvaliabilityService;

describe('ListProviderDayAvaliabilityService', () => {
  beforeEach(() => {
    fakeAppointmentRepository = new FakeAppointmentRepository();
    listProviderDayAvaliabilityService = new ListProviderDayAvaliabilityService(fakeAppointmentRepository);
  });

  it('Listar Horários disponiveis para agendamento no Dia (por Barbeiro)', async () => {

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2020, 4, 20, 14, 0, 0), // 20-04-2020 08:00:00
    });

    await fakeAppointmentRepository.create({
      provider_id: '121',
      user_id: 'user',
      date: new Date(2020, 4, 20, 15, 0, 0), // 20-04-2020 10:00:00
    });

    //Monitora a função NOW da classe Date, sendo que APENAS NA PRIMEIRA chamada da função
    //será retornado a Data customizada, data Atual + 3 horas
    //mockImplementationOnce - Substiutui o retorno da função apenas na primeira vez
    //mockImplementation - Sempre Substiutui o retorno da função
    jest.spyOn(Date, 'now').mockImplementationOnce(() => {
      return new Date(2020, 4, 20, 11).getTime();
    });

    //Consulta os agendamentos do usuário fake '122' na data 20/05/2020
    const availability = await listProviderDayAvaliabilityService.execute({
      provider_id: '121',
      day: 20,
      year: 2020,
      month: 5,
    });

    //console.log(availability);

    //Espera que o retorno seja um array e
    //que os horarios das 8 e 10 estejam com available: false, ocupados
    expect(availability).toEqual(
      expect.arrayContaining([
        { hour: 8, available: false },
        { hour: 9, available: false },
        { hour: 10, available: false },
        { hour: 13, available: true },
        { hour: 14, available: false },
        { hour: 15, available: false },
        { hour: 16, available: true },
      ])
    );
  });
});
