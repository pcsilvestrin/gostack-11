import Appointment from '../infra/typeorm/entities/Appointment';
import { injectable, inject } from 'tsyringe'; //Injeção de Dependência

import { startOfHour, isBefore, getHours, format } from 'date-fns';
import AppError from '@shared/errors/AppError';

import IAppointmentsRepository from '../repositories/IAppointmentsRepository';
import INotificationsRepository from '@modules/notifications/repositories/INotificationRepository';
import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';

interface IRequest {
  provider_id: string;
  user_id: string;
  date: Date;
}

@injectable() //Classe pode receber injeção de Dependência
class CreateAppointmentService{

  //Cria o parametro no Create e com o private, já cria a variável local, tudo de uma vez
  constructor(
    @inject('AppointmentsRepository')
    private appointmentsRespository: IAppointmentsRepository,

    @inject('NotificationsRepository')
    private notificationsRespository: INotificationsRepository,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider,
  ){ }

  public async execute({ provider_id, user_id, date }: IRequest): Promise<Appointment>{
    const appointmentDate = startOfHour(date);

    //Verifica se a data do Agendamento é retroativa a data Atual
    if (isBefore(appointmentDate, Date.now())) {
      throw new AppError('Não é permitido criar agendamentos para datas retroativas.');
    }

    if (user_id === provider_id) {
      throw new AppError('Não é permitido criar agendamentos para o usuário logado.');
    }

    if (getHours(appointmentDate) < 8 || getHours(appointmentDate) > 18) {
      throw new AppError('Não é permitido criar agendamentos com horário fora da faixa das 07:00 até as 17:00.');
    }

    const findAppointmentInSameDate = await this.appointmentsRespository.findByDate(
      appointmentDate,
      provider_id
    );

    if (findAppointmentInSameDate){
      throw new AppError('Já existe um agendamento para este Horário!');
    }

    const appointment = await this.appointmentsRespository.create({
      provider_id,
      user_id,
      date: appointmentDate
    });

    //Cria a notificação para o usuário do Agendamento realizado
    const dateFormated = format(appointmentDate, "dd/MM/yyyy 'ás' HH:mm"); //Formata a Data e Hora
    await this.notificationsRespository.create({
      recipient_id: provider_id,
      content: `Novo agendamento para ${dateFormated}`,
    });

    //Limpa os dados no Redis, utilizando a chave
    await this.cacheProvider.invalidate(`provider-appointments:${provider_id}:${format(appointmentDate, 'yyyy-M-d')}`);

    return appointment;
  }
}

export default CreateAppointmentService;
