import AppError from '@shared/errors/AppError';

import FakeCacheProvider from '@shared/container/providers/CacheProvider/fakes/FakeCacheProvider';
import FakeUsersRepository from '@modules/users/repositories/fakes/FakeUsersRepository';
import ListProvidersService from './ListProvidersService';

let fakeCachProvider: FakeCacheProvider;
let fakeUserRespository : FakeUsersRepository;
let listProvidersService: ListProvidersService;

describe('ListProvidersService', () => {
  beforeEach(() => {
     fakeCachProvider = new FakeCacheProvider();
     fakeUserRespository = new FakeUsersRepository();
     listProvidersService = new ListProvidersService(
       fakeUserRespository,
       fakeCachProvider
     );
  });

  it('Listagem de Usuários', async () => {
    //Cria o 1º Usuário
    const user1 = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Cria o 2º Usuário
    const user2 = await fakeUserRespository.create({
      name: 'Paulo 2',
      email: 'pcsilvestrin@hotmail.com',
      password: '123456',
    });

    //Cria o Usuário que simula estar logado
    const logedUser = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Carrega os usuários com excessão do usuário logado
    const providers = await listProvidersService.execute({
      user_id: logedUser.id,
    });

    //Espera que seja retornado um Array com dois valores, que são
    //os dois primeiros usuários cadastrados
    expect(providers).toEqual([user1, user2]);
  });
});
