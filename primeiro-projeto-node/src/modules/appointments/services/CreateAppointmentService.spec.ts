import AppError from '@shared/errors/AppError';

import FakeCacheProvider from '@shared/container/providers/CacheProvider/fakes/FakeCacheProvider';
import FakeNotificationsRespository from '@modules/notifications/repositories/fakes/FakeNotificationRepository';
import FakeAppointmentRespository from '../repositories/fakes/FakeAppointmentsRepository';
import CreateAppointmentService from './CreateAppointmentService';

let fakeNotificationsRespository: FakeNotificationsRespository;
let fakeAppointmentRespository: FakeAppointmentRespository;
let fakeCachProvider: FakeCacheProvider;
let createAppointmentService: CreateAppointmentService;

describe('CreateAppointment', () => {
  beforeEach(() => {
    fakeCachProvider = new FakeCacheProvider();
    fakeNotificationsRespository= new FakeNotificationsRespository();
    fakeAppointmentRespository = new FakeAppointmentRespository();

    createAppointmentService = new CreateAppointmentService(
      fakeAppointmentRespository,
      fakeNotificationsRespository,
      fakeCachProvider
    );
  });

  it('Criando Appointment', async () => {
    jest.spyOn(Date, 'now').mockImplementationOnce(() => {
      return new Date(2020, 4, 10, 12).getTime();
    });

    const appointment = await createAppointmentService.execute({
      date: new Date(2020, 4, 10, 13),
      user_id: '123320',
      provider_id: '123456'
    });

    expect(appointment).toHaveProperty('id');        //Verifica se Criou a Property ID
    expect(appointment.provider_id).toBe('123456');  //Verifica se o provider_id é o correto

  });

  it('Não permitir criar Appointment para o mesmo dia e hora', async () => {
    //Data 11/02/2021 as 11:00: Hrs
    const appointmentDate = new Date(2021, 2, 11, 11);

    //Cria o primeiro Appointment
    await createAppointmentService.execute({
      provider_id: '123456',
      date: appointmentDate,
      user_id: '656556'
    });

    //Ao criar o segundo Appointment com a mesma data e hora,
    //é esperado que retorno um erro e esse erro deve ser do tipo AppError
    expect(
      createAppointmentService.execute({
        provider_id: '123456',
        date: appointmentDate,
        user_id: '656556'
      })
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Não permitir criar agendamento para datas retroativas', async () => {
    jest.spyOn(Date, 'now').mockImplementationOnce(() => {
      return new Date(2020, 4, 10, 12).getTime();
    });

    await expect(
      createAppointmentService.execute({
        date: new Date(2020, 4, 10, 11),
        user_id: '123456',
        provider_id: '121212',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Não permitir criar agendamento quando o usuário logado também for o provider', async () => {
    jest.spyOn(Date, 'now').mockImplementationOnce(() => {
      return new Date(2020, 4, 10, 12).getTime();
    });

    await expect(
      createAppointmentService.execute({
        date: new Date(2020, 4, 10, 11),
        user_id: '123456',
        provider_id: '123456',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Não permitir criar agendamento quando o usuário logado também for o provider', async () => {
    await expect(
      createAppointmentService.execute({
        date: new Date(2025, 4, 10, 11),
        user_id: '123456',
        provider_id: '123456',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Não permitir criar agendamento quando o horário for antes das 7 e depois das 18', async () => {
    jest.spyOn(Date, 'now').mockImplementationOnce(() => {
      return new Date(2020, 4, 10, 12).getTime();
    });

    await expect(
      createAppointmentService.execute({
        date: new Date(2020, 4, 11, 7),
        user_id: '1234567',
        provider_id: '123456',
      }),
    ).rejects.toBeInstanceOf(AppError);

    await expect(
      createAppointmentService.execute({
        date: new Date(2020, 4, 11, 18),
        user_id: '1234567',
        provider_id: '123456',
      }),
    ).rejects.toBeInstanceOf(AppError);

  });
});
