import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import { getDate, getDaysInMonth, isAfter } from 'date-fns';
import IAppointmentsRepository from '../repositories/IAppointmentsRepository';

interface IRequest{
  provider_id: string;
  month : number;
  year  : number;
}

type IResponse = Array<{
  day: number;
  available: boolean; //Disponivel
}>;

@injectable()
class ListProviderMonthAvaliabilityService {
  constructor(
     @inject('AppointmentsRepository')
     private appointmentRepository: IAppointmentsRepository,
  ){}

  public async execute({ provider_id, year, month }: IRequest): Promise<IResponse>{
    const appointments = await this.appointmentRepository.findAllInMonthFromProvider({
      provider_id,
      year,
      month
    });

    //Captura o número total de dias que o mês contém
    const numberOfDaysInMonth = getDaysInMonth(
      new Date(year, month -1)
    );

    //Gera um Array com todos os dias do Mês
    const eachDayArray = Array.from(
      { length: numberOfDaysInMonth }, //Define o Tamanho do Array
      (value, index) => index +1,
    );

    const availability = eachDayArray.map(day => {
      //MÊS começa do Zero
      const compareDate = new Date(year, month - 1, day, 23, 59, 59);

      //Carrega os Agendamentos para o Dia
      const appointmentsInDay = appointments.filter(appointment => {
        return getDate(appointment.date) === day;
      });

      return {
        day,
        available: isAfter(compareDate, new Date()) &&  //Verifica se a Data de Agendamento é menor qua a data atual
                   appointmentsInDay.length < 10,       //Máximo 10 Agendamentos por Dia
      };
    });

    return availability;
  }
}

export default ListProviderMonthAvaliabilityService;

