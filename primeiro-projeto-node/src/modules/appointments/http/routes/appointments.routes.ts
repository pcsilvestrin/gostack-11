import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';

import AppointmentsController from '../controllers/AppointmentsController';
import ProviderAppointmentsController from '../controllers/ProviderAppointmentsController';

const appointmentsRouter = Router();

const appointmentsController = new AppointmentsController();
const providerAppointmentsController = new ProviderAppointmentsController();

//Aplica a validação do Token em TODAS as Rotas
appointmentsRouter.use(ensureAuthenticated);

appointmentsRouter.post('/', celebrate({
  [Segments.BODY]: { //Aplica a validação nos dados passados no Body da Requisição
    provider_id: Joi.string().uuid(), //Espera que exista o elemento 'provider_id' e seja do tipo string e uuid
    date: Joi.date(), //Espera que exista o elemento 'date' e seja do tipo Date
  }
}), appointmentsController.create);

appointmentsRouter.get('/me', providerAppointmentsController.index); //Carrega os Agendamentos do Usuário

export default appointmentsRouter;
