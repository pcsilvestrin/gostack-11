import { Request, Response } from 'express';
import { container } from 'tsyringe'; //Injeção de Dependência

import ListProvideDayAvailibilityService from '@modules/appointments/services/ListProviderDayAvaliabilityService';

export default class ProviderDayAvailabilityController {

  public async index(request: Request, response: Response): Promise<Response> {
    const { provider_id } = request.params; //Parametro passado na Rota providers.routes.ts
    const { day, month, year } = request.query;

    const listProviderDayAvaliability = container.resolve(ListProvideDayAvailibilityService);

    const availability = await listProviderDayAvaliability.execute({
      provider_id,
      day: Number(day),
      month: Number(month),
      year: Number(year)
    });

    return response.json(availability);
  }
}
