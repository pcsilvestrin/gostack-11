import { Request, Response } from 'express';
import { container } from 'tsyringe'; //Injeção de Dependência

import ListProvideMonthAvailibilityService from '@modules/appointments/services/ListProviderMonthAvaliabilityService';

export default class ProviderMonthAvailabilityController {

  public async index(request: Request, response: Response): Promise<Response> {
    const { provider_id } = request.params; //Parametro passado na Rota providers.routes.ts
    const { month, year } = request.query;

    const listProviderMonthAvaliability = container.resolve(ListProvideMonthAvailibilityService);

    const availability = await listProviderMonthAvaliability.execute({
      provider_id,
      month: Number(month),
      year: Number(year)
    });

    return response.json(availability);
  }
}
