import { ObjectID,
         Entity,
         Column,
         CreateDateColumn,
         UpdateDateColumn,
         ObjectIdColumn
} from 'typeorm';

@Entity('notifications')
class Notification {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  content: string; //Conteúdo da Notificação

  @Column('uuid')
  recipient_id: string; //ID do Usuário que irá receber a notificação

  //@Column({ default: false })
  read: boolean; //Se a mensagem já foi lida

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Notification;
