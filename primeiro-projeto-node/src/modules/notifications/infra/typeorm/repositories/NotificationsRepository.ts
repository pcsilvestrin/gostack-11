import Notification from '../schemas/Notification';
import { getMongoRepository, MongoRepository } from 'typeorm';

import INotificationRepository from '@modules/notifications/repositories/INotificationRepository';
import ICreateNotificationDTO from '@modules/notifications/dtos/ICreateNotificationDTO';

class NotificationsRespository implements INotificationRepository {
  private ormRepository: MongoRepository<Notification>;

  constructor(){
    this.ormRepository = getMongoRepository(Notification, 'mongo');
  }

  public async create({ content, recipient_id }: ICreateNotificationDTO): Promise<Notification> {
    //Cria o objeto Appointment
    const notification = this.ormRepository.create({ content, recipient_id });

    //Salva o objeto no Banco
    await this.ormRepository.save(notification);

    return notification;
  }
}

export default NotificationsRespository;
