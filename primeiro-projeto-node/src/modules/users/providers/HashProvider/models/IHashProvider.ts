export default interface IHashProvider {
  generateHash(payload: String): Promise<string>;
  compareHash(payload: String, hashed: string): Promise<boolean>;
}
