import { Request, Response, NextFunction, request } from 'express';
import { verify } from 'jsonwebtoken';

import authConfig from '@config/auth';
import AppError from '@shared/errors/AppError';

interface TokenPayLoad {
  iat: number,
  exp: number,
  sub: string,
}

export default function ensureAuthenticated(
  request: Request,
  response: Response,
  next: NextFunction
): void{

  //Captura o Token enviado no Cabeçalho da Requisição
  const authHeader = request.headers.authorization;

  if (!authHeader){
    throw new AppError('Token JWT não informado!', 401);
  }

  //Na requisição, antes do token vem a palavra 'bearer'
  //o split gera um array quebrando a string no Espaço
  //esse array vai possuir dois elementos,
  //na linha abaixo está sendo desestruturado e está sendo pego apenas o token
  const [,token] = authHeader.split(' ');

  try {
    //Utilizando o metodo verify(), é possível validar a estrutura do Token
    const decoded = verify(token, authConfig.jwt.secret);

    //Seta para o sub um objeto do tipo TokenPayLoad
    //O 'as' serve para forçar o TS a ententer o 'decoded' como um TokenPayLoad
    const { sub } = decoded as TokenPayLoad;

    //Seta o código do usuário que veio no Token para dentro do Request
    request.user = {
      id: sub,
    }

    return next();
  } catch {
    throw new AppError('O Token informado é inválido!', 401);
  }
}
