import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';
import multer from 'multer';
import uploadConfig from '@config/upload';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';
import UsersController from '../controllers/UsersController';
import UserAvatarController from '../controllers/UserAvatarController';

const usersRouter = Router();

const usersController = new UsersController();
const userAvatarController = new UserAvatarController();

//upload se torna um multer, podendo ser definido se será feito o upload
//de um ou mais arquivos
const upload = multer(uploadConfig.multer);

usersRouter.post('/', celebrate({
  [Segments.BODY]: {
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  },
}), usersController.create);

//Rota utilizada para atualização do Avatar
//Exige autênticação do Usuário
usersRouter.patch('/avatar',
                   ensureAuthenticated,
                   upload.single('avatar'),
                   userAvatarController.update);

export default usersRouter;
