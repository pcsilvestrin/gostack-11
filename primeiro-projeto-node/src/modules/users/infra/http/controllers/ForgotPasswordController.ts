import { Request, Response } from 'express';
import { container } from 'tsyringe'; //Injeção de Dependência

import SendForgotPasswordEmailController from '@modules/users/services/SendForgotPasswordEmailService';

export default class ForgotPasswordController {
  public async create(request: Request, response: Response): Promise<Response>{
    const { email } = request.body;

    const sendForgotPasswordEmailController = container.resolve(
      SendForgotPasswordEmailController //Injeção de Dependência
    );

    await sendForgotPasswordEmailController.execute({
      email,
    });

    return response.status(204).json();
  }
}
