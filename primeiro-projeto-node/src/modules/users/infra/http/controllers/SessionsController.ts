import { Request, Response } from 'express';
import AuthenticateUserService from '@modules/users/services/AuthenticateUserService';
import { container } from 'tsyringe'; //Injeção de Dependência
import { classToClass } from 'class-transformer';

export default class SessionsController {
  public async create(request: Request, response: Response): Promise<Response>{
    const { email, password } = request.body;

    const authenticateUserService = container.resolve(AuthenticateUserService); //Injeção de Dependência

    const { user, token } = await authenticateUserService.execute({
      email,
      password
    });

    //Para não retornar a senha do usuário, deve ser instanciado outro user, sem senha
    /*const userWithoutPassword = {
      id: user.id,
      name: user.name,
      email: user.email,
      created_at: user.created_at,
      updated_at: user.updated_at,
    }*/

    //Retorna os dados do Usuário e o Token gerado
    return response.json({ user: classToClass(user), token });
  }
}
