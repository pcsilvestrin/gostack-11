import { Request, Response } from 'express';
import { container } from 'tsyringe'; //Injeção de Dependência
import { classToClass } from 'class-transformer';

import UpdateProfileService from '@modules/users/services/UpdateProfileService';
import ShowProfileService from '@modules/users/services/ShowProfileService';

export default class ProfileController {

  //Rota para listar os dados do Usuário
  public async show(request: Request, response: Response): Promise<Response>{
    const user_id = request.user.id; //Carrega o ID do usuário logado - Middleware alimenta o ID

    const showProfile = container.resolve(ShowProfileService);

    const user = await showProfile.execute({ user_id });

    //user.password = ''; //Para não retornar na requisição

    return response.json(classToClass(user));
  }

  //Rota para atualizar os dados do usuário
  public async update(request: Request, response: Response): Promise<Response | null>{
    const user_id = request.user.id; //Carrega o ID do usuário logado - Middleware alimenta o ID
    const { name, email, old_password, password } = request.body;

    const updateProfile = container.resolve(UpdateProfileService);

    const user = await updateProfile.execute({
      user_id,
      name,
      email,
      old_password,
      password,
    });

    //user.password = ''; //Para não retornar na requisição

    return response.json(classToClass(user));
  }
}
