import { Request, Response } from 'express';
import { container } from 'tsyringe'; //Injeção de Dependência
import { classToClass } from 'class-transformer';

import CreateUserService from '@modules/users/services/CreateUserService';

export default class UsersController {
  public async create(request: Request, response: Response): Promise<Response>{
    const { name, email, password } = request.body;

    const createUser = container.resolve(CreateUserService); //Injeção de Dependência

    //Chama a classe service e salva o Objeto
    const user = await createUser.execute({
      name,
      email,
      password,
    });

    //Para não retornar a senha do usuário, deve ser instanciado outro user, sem senha
    /*const userWithoutPassword = {
      id: user.id,
      name: user.name,
      email: user.email,
      created_at: user.created_at,
      updated_at: user.updated_at,
    }*/

    return response.json(classToClass(user));
  }
}
