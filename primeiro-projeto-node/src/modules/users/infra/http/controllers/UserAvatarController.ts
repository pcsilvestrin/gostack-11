import { Request, Response } from 'express';
import { container } from 'tsyringe'; //Injeção de Dependência
import { classToClass } from 'class-transformer';

import UpdateUserAvatarService from '@modules/users/services/UpdateUserAvatarService';

export default class UserAvatarController {
  public async update(request: Request, response: Response): Promise<Response>{
    const updateUserAvatar = container.resolve(UpdateUserAvatarService); //Injeção de Dependência

    const userUpdate = await updateUserAvatar.execute({
      user_id: request.user.id, //Pega o ID do usuário que fez a requisição
      avatarFileName: request.file.filename, //Captura o nome do arquivo que foi recebido na requisição
    });

    return response.json(classToClass(userUpdate));
  }
}
