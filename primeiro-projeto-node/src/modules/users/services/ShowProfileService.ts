import AppError from '@shared/errors/AppError';
import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import User from '../infra/typeorm/entities/User';

import IHashProvider from '../providers/HashProvider/models/IHashProvider';
import IUserRepository from '../repositories/IUserRepository';

interface IRequest{
  user_id: string;
}

@injectable()
class ShowProfileService {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUserRepository,
  ){}

  public async execute({ user_id }: IRequest): Promise<User>{

    const user = await this.usersRepository.findById(user_id);
    if (!user){
      throw new AppError('Usuário não cadastrado!');
    }

    return user;
  }
}

export default ShowProfileService;
