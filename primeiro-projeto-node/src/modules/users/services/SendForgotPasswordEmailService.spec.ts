import AppError from '@shared/errors/AppError';

import FakeUsersRespository from '../repositories/fakes/FakeUsersRepository';
import FakeEmailProvider from '@shared/container/providers/MailProvider/fakes/FakeMailProvider';

import SendForgotPasswordEmailService from './SendForgotPasswordEmailService';
import FakeUserTokensRepository from '../repositories/fakes/FakeUserTokensRepository';

//Declara os elementos que são utilizados em todos os testes
let fakeUserRespository: FakeUsersRespository;
let fakeEmailProvider: FakeEmailProvider;
let fakeUserTokensRepository: FakeUserTokensRepository;
let sendForgotPasswordEmail: SendForgotPasswordEmailService;

describe('SendForgotPasswordEmail', () => {

  //Antes de executar cada teste, é instanciado os elementos
  beforeEach(() => {
    fakeUserRespository = new FakeUsersRespository();
    fakeEmailProvider = new FakeEmailProvider();
    fakeUserTokensRepository = new FakeUserTokensRepository();

    sendForgotPasswordEmail = new SendForgotPasswordEmailService(
      fakeUserRespository,
      fakeEmailProvider,
      fakeUserTokensRepository
    );
  });

  it('Realizar o envio de email para o usuário para recuperação de senha', async () => {
    //Monitora a rotina sendMail do FakeEmailProvider, para ver se ela foi executada
    const sendEmail = jest.spyOn(fakeEmailProvider, 'sendMail');

    //Cria o usuário para teste
    await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Faz o teste de Envio de Email
    await sendForgotPasswordEmail.execute({
      email: 'pcsilvestrin@gmail.com',
    });

    expect(sendEmail).toHaveBeenCalled(); //Verifica se Passou pela Rotina
  });

  it('Não permitir solicitar a senha quando o usuário informado não estiver cadastrado', async () => {
    //Passa a senha de um usuário que não está cadastrado, esperando um erro
    await expect(
      sendForgotPasswordEmail.execute({
        email: 'email_nao_cadastrado@gmail.com',
      })
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Gerar Token com validade de 2 horas para poder recuperar a senha', async () => {
    //Monitora a rotina generate do fakeUserTokensRepository, para ver se ela foi executada
    const generateToken = jest.spyOn(fakeUserTokensRepository, 'generate');

    //Cria o usuário para teste
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Faz o teste de Envio de Email
    await sendForgotPasswordEmail.execute({
      email: 'pcsilvestrin@gmail.com',
    });

    //Verifica se Passou pela Rotina 'generate' e o parametro foi o id do usuário
    expect(generateToken).toHaveBeenCalledWith(user.id);
  });
});
