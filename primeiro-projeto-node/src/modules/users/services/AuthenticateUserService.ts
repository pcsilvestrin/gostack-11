import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import { sign } from 'jsonwebtoken';

import authConfig from '../../../config/auth';
import AppError from '@shared/errors/AppError';

import User from '../infra/typeorm/entities/User';

import IUserRepository from '../repositories/IUserRepository';
import IHashProvider from '../providers/HashProvider/models/IHashProvider';

interface IRequest {
  email: string;
  password: string;
}

interface IResponse {
  user: User,
  token: string
}

@injectable() //Classe pode receber injeção de Dependência
class AuthenticateUserService{
  //Cria o parametro no Create e com o private, já cria a variável local, tudo de uma vez
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUserRepository,

    @inject('HashProvider')
    private hashProvier: IHashProvider
  ){ }

  public async execute({email, password}: IRequest): Promise<IResponse>{

    const user = await this.usersRepository.findByEmail(email);

    if (!user){
      throw new AppError('O Email ou Senha está incorreto!', 401);
    }

    //Verifica se a senha passada na requisição é a mesma senha salva no banco
    //por mais que a senha esteja criptografada no banco, o metodo 'compare()' do bcryptjs
    //consegue fazer a comparação
    const passwordMatched = await this.hashProvier.compareHash(password, user.password);

    if (!passwordMatched){
      throw new AppError('O Email ou Senha está incorreto!', 401);
    }

    //Carrega as configurações do JWT do arquivo de configurações
    const { secret, expiresIn } = authConfig.jwt;

    //1º Parametro: Informações que podem ser acessadas por quem fez a requisição, como inf. do usuário
    //2º Parametro: Palavra chave para gerar a Criptografia
    //3º Parametro: Configurações Adicionais
    const token = sign({}, secret, {
      subject: user.id, /*Sempre será o ID do usuário que gerou o Token*/
      expiresIn,  /*Define o período em que o Token será válido*/
    });

    return {
      user,
      token,
    };
  }
}

export default AuthenticateUserService;
