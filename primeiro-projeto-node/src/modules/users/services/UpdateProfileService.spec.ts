import AppError from '@shared/errors/AppError';

import FakeHashProvider from '../providers/HashProvider/fakes/FakeHashProvider';
import FakeUsersRepository from '../repositories/fakes/FakeUsersRepository';
import UpdateProfileService from './UpdateProfileService';

let fakeUserRespository: FakeUsersRepository;
let fakeHahsProvider: FakeHashProvider;
let updateProfileService: UpdateProfileService;

describe('UpdateProfile', () => {
  beforeEach(() => {
     fakeUserRespository = new FakeUsersRepository();
     fakeHahsProvider = new FakeHashProvider();

     updateProfileService = new UpdateProfileService(
       fakeUserRespository,
       fakeHahsProvider,
     );
  });

  it('Realizar Atualização do Profile do usuário', async () => {

    //Cria o usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Insere a imagem Avatar para o usuário pelo ID
    const updatedUser = await updateProfileService.execute({
      user_id: user.id,
      name: 'Usuário Teste',
      email: 'pcsilvestrin@hotmail.com',
    });

    //Verifica se o Avatar foi salvo com sucesso
    expect(user.name).toBe('Usuário Teste');
    expect(user.email).toBe('pcsilvestrin@hotmail.com');
  });

  it('Não permitir informar um email já cadastrado', async () => {

    //Cria o usuário
    await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    const user = await fakeUserRespository.create({
      name: 'Pedro',
      email: 'pedro@gmail.com',
      password: '123456',
    });

    await expect(
      updateProfileService.execute({
        user_id: user.id,
        name: 'Usuário Teste',
        email: 'pcsilvestrin@gmail.com',
      })
    ).rejects.toBeInstanceOf(AppError);

  });

  it('Atualizar a senha do usuário', async () => {

    //Cria o usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    const updatedUser = await updateProfileService.execute({
      user_id: user.id,
      name: 'Usuário Teste',
      email: 'pcsilvestrin@hotmail.com',
      old_password: '123456',
      password: '123123',
    });

    expect(updatedUser.password).toBe('123123');
  });

  it('Não permitir atualizar a senha do usuário, sem informar a senha antiga', async () => {
    //Cria o usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    await expect(
      updateProfileService.execute({
        user_id: user.id,
        name: 'Usuário Teste',
        email: 'pcsilvestrin@hotmail.com',
        password: '123123',
      })
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Não permitir atualizar a senha do usuário, quando a senha antiga estiver errada', async () => {
    //Cria o usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    await expect(
      updateProfileService.execute({
        user_id: user.id,
        name: 'Usuário Teste',
        email: 'pcsilvestrin@hotmail.com',
        password: '123123',
        old_password: '333333'
      })
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Usuário Não cadastrado', async () => {
    await expect(
      updateProfileService.execute({
        user_id: 'user-not-exist',
        name: 'Teste',
        email: 'teste@gmail.com'
      })
    ).rejects.toBeInstanceOf(AppError);
  });
});
