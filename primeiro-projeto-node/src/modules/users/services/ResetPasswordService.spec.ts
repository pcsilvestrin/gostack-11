import AppError from '@shared/errors/AppError';
import FakeHashProvider from '../providers/HashProvider/fakes/FakeHashProvider';

import FakeUsersRespository from '../repositories/fakes/FakeUsersRepository';

import FakeUserTokensRepository from '../repositories/fakes/FakeUserTokensRepository';
import ResetPasswordService from './ResetPasswordService';

//Declara os elementos que são utilizados em todos os testes
let fakeUserRespository: FakeUsersRespository;
let fakeUserTokensRepository: FakeUserTokensRepository;
let resetPassword: ResetPasswordService;
let fakehashProvider: FakeHashProvider;

describe('ResetPassword', () => {

  //Antes de executar cada teste, é instanciado os elementos
  beforeEach(() => {
    fakeUserRespository = new FakeUsersRespository();
    fakeUserTokensRepository = new FakeUserTokensRepository();
    fakehashProvider = new FakeHashProvider();

    resetPassword = new ResetPasswordService(
      fakeUserRespository,
      fakeUserTokensRepository,
      fakehashProvider
    );
  });

  it('Resetar a senha do usuário', async () => {

    //Cria o usuário para teste
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Gera o Token de Recuperação de senha
    const { token } = await fakeUserTokensRepository.generate(user.id);

    //Monitora a rotina 'generateHash' do 'fakehashProvider'
    const generateHash = jest.spyOn(fakehashProvider, 'generateHash');

    //Faz o teste de Envio de Email
    await resetPassword.execute({
      password: '123123',
      token
    });

    //Carrega o usuário novamente
    const updatedUser = await fakeUserRespository.findById(user.id);

    //Espera que tenha passado pela rotina generateHash com o paramento '123123'
    expect(generateHash).toHaveBeenCalledWith('123123');

    //Verifica se a Senha foi alterada
    expect(updatedUser?.password).toBe('123123');
  });

  it('Não permitir Resetar a Senha quando o TOKEN for inexistente', async () => {
    await expect(
      resetPassword.execute({
        token: 'no-valid',
        password: '123',
      })
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Não permitir Resetar a Senha quando o USUÁRIO for inexistente', async () => {
    const { token } = await fakeUserTokensRepository.generate('user-not-exist');

    await expect(
      resetPassword.execute({
        token,
        password: '123',
      })
    ).rejects.toBeInstanceOf(AppError);
  });

  it('Não Resetar a senha do usuário quando o Token estiver expirado (2 Horas)', async () => {
    //Cria o usuário para teste
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Gera o Token de Recuperação de senha
    const { token } = await fakeUserTokensRepository.generate(user.id);

    //Monitora a função NOW da classe Date, sendo que APENAS NA PRIMEIRA chamada da função
    //será retornado a Data customizada, data Atual + 3 horas
    //mockImplementationOnce - Substiutui o retorno da função apenas na primeira vez
    //mockImplementation - Sempre Substiutui o retorno da função
    jest.spyOn(Date, 'now').mockImplementationOnce(() => {
      const customDate = new Date();

      return customDate.setHours(customDate.getHours() + 3);
    });

    await expect(
      resetPassword.execute({
        password: '1236',
        token
      })
    ).rejects.toBeInstanceOf(AppError);
  });

});
