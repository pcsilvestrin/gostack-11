import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import path from 'path';

import AppError from '@shared/errors/AppError';

import IMailProvider from '@shared/container/providers/MailProvider/models/IMailProvider';
import IUserRepository from '../repositories/IUserRepository';
import IUserTokensRepository from '../repositories/IUserTokensRepository';

interface IRequest {
  email: string;
}

@injectable() //Classe pode receber injeção de Dependência
class SendForgotPasswordEmailService{

  //Cria o parametro no Create e com o private, já cria a variável local, tudo de uma vez
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUserRepository,

    @inject('MailProvider')
    private mailProvider: IMailProvider,

    @inject('UserTokensRepository')
    private userTokensRepository: IUserTokensRepository,
  ){ }

  public async execute({ email }: IRequest): Promise<void>{
    const user = await this.usersRepository.findByEmail(email);

    if (!user){
      throw new AppError('Usuário não existe.');
    }

    const { token } = await this.userTokensRepository.generate(user.id);

    //Gera o path de onde o arquivo está salvo
    const forgotTemplatePassword = path.resolve(
      __dirname,
      '..',
      'views',
      'forgot_password.hbs'
    )

    await this.mailProvider.sendMail({
      //Gera as informações para poder enviar o email
      to: {
        name: user.name,
        email: user.email,
      },
      subject: '[GoBarber] Recuperação de Senha',
      templateData: {
        //Envia o path, para que dentro do HandlebarsMailTemplateProvider, seja carregado o Template
        //e realizado a manipulação das Variáveis
        file: forgotTemplatePassword,
        variables: {
          name: user.name,
          link: `${process.env.APP_WEB_URL}/reset-password/?token=${token}`,
        },
      },
    });
  }
}

export default SendForgotPasswordEmailService;
