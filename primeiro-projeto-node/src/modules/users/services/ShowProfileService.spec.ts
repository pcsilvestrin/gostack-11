import AppError from '@shared/errors/AppError';

import FakeUsersRepository from '../repositories/fakes/FakeUsersRepository';
import ShowProfileService from './ShowProfileService';
import UpdateProfileService from './UpdateProfileService';

let fakeUserRespository: FakeUsersRepository;
let showProfileService: ShowProfileService;

describe('ShowProfile', () => {
  beforeEach(() => {
     fakeUserRespository = new FakeUsersRepository();
     showProfileService = new ShowProfileService(fakeUserRespository);
  });

  it('Usuário cadastrado', async () => {
    //Cria o usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    const profile = await showProfileService.execute({
      user_id: user.id
    });

    expect(profile.name).toBe('Paulo');
    expect(profile.email).toBe('pcsilvestrin@gmail.com');
  });

  it('Usuário Não cadastrado', async () => {
    await expect(
      showProfileService.execute({
        user_id: 'user-not-exist'
      })
    ).rejects.toBeInstanceOf(AppError);
  });
});
