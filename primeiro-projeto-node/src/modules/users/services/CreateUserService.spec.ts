import AppError from '@shared/errors/AppError';

import FakeCacheProvider from '@shared/container/providers/CacheProvider/fakes/FakeCacheProvider';
import FakeUsersRespository from '../repositories/fakes/FakeUsersRepository';
import FakeHashProvider from '../providers/HashProvider/fakes/FakeHashProvider';

import CreateUserService from './CreateUserService';

let fakeCachProvider: FakeCacheProvider;
let fakeUserRespository: FakeUsersRespository;
let fakeHashProvider: FakeHashProvider;
let createUserService: CreateUserService;

describe('CreateUser', () => {
  beforeEach(() => {
    fakeCachProvider = new FakeCacheProvider();
    fakeUserRespository = new FakeUsersRespository();
    fakeHashProvider = new FakeHashProvider();

    createUserService = new CreateUserService(
      fakeUserRespository,
      fakeHashProvider,
      fakeCachProvider
    );
  });

  it('Criando Usuário', async () => {
    const user = await createUserService.execute({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    expect(user).toHaveProperty('id'); //Verifica se Criou a Property ID
  });

  it('Não permitir criar Usuário com mesmo email', async () => {
    await createUserService.execute({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Espera que de ERRO, pois está criando um usuário com um email já cadastrado
    await expect(
      createUserService.execute({
        name: 'Paulo',
        email: 'pcsilvestrin@gmail.com',
        password: '123456',
      })
    ).rejects.toBeInstanceOf(AppError);
  });
});
