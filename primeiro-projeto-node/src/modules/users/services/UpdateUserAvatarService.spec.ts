import AppError from '@shared/errors/AppError';

import FakeStorageProvider from '@shared/container/providers/StorageProvider/fakes/FakesDiskStorageProvider';
import FakeUsersRepository from '../repositories/fakes/FakeUsersRepository';

import UpdateUserAvatarService from './UpdateUserAvatarService';

let fakeUserRespository: FakeUsersRepository;
let fakeStorageProvider: FakeStorageProvider;
let updateUserAvatar: UpdateUserAvatarService;

describe('UpdateUserAvatar', () => {
  beforeEach(() => {
     fakeUserRespository = new FakeUsersRepository();
     fakeStorageProvider = new FakeStorageProvider();

     updateUserAvatar = new UpdateUserAvatarService(
       fakeUserRespository,
       fakeStorageProvider,
     );
  });

  it('Realizar Upload do Avatar do Usuário', async () => {
    //Cria o usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Insere a imagem Avatar para o usuário pelo ID
    await updateUserAvatar.execute({
      user_id: user.id,
      avatarFileName: 'avatar.png',
    })

    //Verifica se o Avatar foi salvo com sucesso
    expect(user.avatar).toBe('avatar.png');
  });

  it('Não permitir realizar upload quando o ID do usuário não estiver cadastrado', async () => {
    await expect(
      updateUserAvatar.execute({
        user_id: 'invalid-id',
        avatarFileName: 'avatar.png',
      })
    ).rejects.toBeInstanceOf(AppError);
  });


  it('Excluir o Avatar antigo do Usuário, quando for realizado o upload de um novo Avatar', async () => {
    //Monitora a função 'deleteFile' para verificar se o teste passou por ela
    const deleteFile = jest.spyOn(fakeStorageProvider, 'deleteFile');

    //Cria o usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    //Insere a imagem Avatar para o usuário pelo ID
    await updateUserAvatar.execute({
      user_id: user.id,
      avatarFileName: 'avatar.png',
    });

    //Atualiza o Avatar para o usuário pelo ID
    await updateUserAvatar.execute({
      user_id: user.id,
      avatarFileName: 'avatar2.png',
    })

    //Verifica se o Teste passou pela rotina de DeleteFile com o parametro 'avatar.png'
    expect(deleteFile).toHaveBeenCalledWith('avatar.png');
    expect(user.avatar).toBe('avatar2.png'); //Verifica se o Avatar do Usuário está correto
  });

});
