import FakeCacheProvider from '@shared/container/providers/CacheProvider/fakes/FakeCacheProvider';
import FakeUsersRespository from '../repositories/fakes/FakeUsersRepository';
import FakeHashProvider from '../providers/HashProvider/fakes/FakeHashProvider';

import CreateUserService from './CreateUserService';
import AuthenticateUserService from './AuthenticateUserService';
import AppError from '@shared/errors/AppError';

let fakeCachProvider: FakeCacheProvider;
let fakeUserRespository: FakeUsersRespository;
let fakeHashProvider: FakeHashProvider;
let authenticateUser: AuthenticateUserService;

describe('AuthenticateUser', () => {
  beforeEach(() => {
    fakeCachProvider = new FakeCacheProvider();
    fakeUserRespository = new FakeUsersRespository();
    fakeHashProvider = new FakeHashProvider();

    authenticateUser = new AuthenticateUserService(fakeUserRespository, fakeHashProvider);
  });

  it('Autenticando Usuário', async () => {
    //Primeiro cria o Usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    })

    //Para que possa ser verificado se ele está Cadastrado
    const response = await authenticateUser.execute({
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    });

    expect(response).toHaveProperty('token'); //Verifica se na resposta veio o Token
    expect(response.user).toEqual(user); //Verifica se o usuário retornado é o mesmo criado anteriormente
  });

  it('Não permitir autenticar Usuário sem cadastro', async () => {
    await expect(
      authenticateUser.execute({
        email: 'pcsilvestrin@gmail.com',
        password: '123456',
      })
    ).rejects.toBeInstanceOf(AppError);  //Verifica se gerou o ERRO assim como esperado
  });

  it('Não permitir autenticar Usuário com email ou senha inválido', async () => {
    //Primeiro cria o Usuário
    const user = await fakeUserRespository.create({
      name: 'Paulo',
      email: 'pcsilvestrin@gmail.com',
      password: '123456',
    })

    await expect(
      authenticateUser.execute({
        email:  'pcsilvestrin@gmail.com',
        password: 'password-invalid',
      })
    ).rejects.toBeInstanceOf(AppError);  //Verifica se gerou o ERRO assim como esperado
  });
});
