import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import AppError from '@shared/errors/AppError';

import User from '../infra/typeorm/entities/User';

import IUserRepository from '../repositories/IUserRepository';
import IStorageProvider from '@shared/container/providers/StorageProvider/models/IStorageProvider';


interface IRequest{
  user_id: string
  avatarFileName: string;
}

@injectable() //Classe pode receber injeção de Dependência
class UpdateUserAvatarService {
  //Cria o parametro no Create e com o private, já cria a variável local, tudo de uma vez
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUserRepository,

    @inject('StorageProvider')
    private storageProvider: IStorageProvider
  ){ }

  public async execute({ user_id, avatarFileName }: IRequest): Promise<User>{
    //Busa o usuário pelo ID
    const user = await this.usersRepository.findById(user_id);

    if (!user){
      throw new AppError('Usuário não encontrado!', 401);
    }

    //Se o usuário já possuir o Avatar, deve ser excluido a imagem que está
    //vinculada ao usuário, da pasta tmp
    if (user.avatar){
      await this.storageProvider.deleteFile(user.avatar);
    }

    const fileName = await this.storageProvider.saveFile(avatarFileName);

    user.avatar = fileName; //Seta o endereço da nova imagem

    await this.usersRepository.save(user); //Atualiza o cadastro do usuário

    user.password = '';
    return user;
  }
}

export default UpdateUserAvatarService;
