import { injectable, inject } from 'tsyringe'; //Injeção de Dependência

import AppError from '@shared/errors/AppError';

import User from "../infra/typeorm/entities/User";
import IUserRepository from '../repositories/IUserRepository';
import IHashProvider from '../providers/HashProvider/models/IHashProvider';
import ICacheProvider from '@shared/container/providers/CacheProvider/models/ICacheProvider';

interface IRequest {
  name: string;
  email: string;
  password: string;
}

@injectable() //Classe pode receber injeção de Dependência
class CreateUserService{

  //Cria o parametro no Create e com o private, já cria a variável local, tudo de uma vez
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUserRepository,

    @inject('HashProvider')
    private hashProvider: IHashProvider,

    @inject('CacheProvider')
    private cacheProvider: ICacheProvider
  ){ }

  public async execute({ name, email, password }: IRequest): Promise<User>{
    const chekUsersExists = await this.usersRepository.findByEmail(email);

    if (chekUsersExists){
      throw new AppError('O email informado já está sendo utilizado!');
    }

    //8 é o valor da chave que será utilizado na Criptografia, pode ser
    //informado numeros ou letras
    const hashedPassword = await this.hashProvider.generateHash(password);

    const user = await this.usersRepository.create({
      name,
      email,
      password: hashedPassword,
    });

    //Exclui do Redis todas as listas de usuários, para que possam ser
    //recriadas ao realizar a consulta de Providers
    await this.cacheProvider.invalidatePrefix('providers-list');

    return user;
  }
}

export default CreateUserService;
