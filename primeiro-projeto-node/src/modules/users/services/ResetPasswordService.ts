import AppError from '@shared/errors/AppError';
import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import { addHours, isAfter } from 'date-fns';
import IHashProvider from '../providers/HashProvider/models/IHashProvider';

//import AppError from '@shared/errors/AppError';

import IUserRepository from '../repositories/IUserRepository';
import IUserTokensRepository from '../repositories/IUserTokensRepository';

interface IRequest {
  token: string;
  password: string;
}

@injectable() //Classe pode receber injeção de Dependência
class ResetPasswordService{

  //Cria o parametro no Create e com o private, já cria a variável local, tudo de uma vez
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUserRepository,

    @inject('UserTokensRepository')
    private userTokensRepository: IUserTokensRepository,

    @inject('HashProvider')
    private hashProvider: IHashProvider,
  ){ }

  public async execute({ token, password }: IRequest): Promise<void>{

    const userToken = await this.userTokensRepository.findByToken(token);
    if (!userToken){
      throw new AppError('Token informado não existe!');
    }

    const user = await this.usersRepository.findById(userToken.user_id);
    if (!user){
      throw new AppError('Usuário não existe!');
    }

    const tokenCreated = userToken.created_at;
    const compareDates = addHours(tokenCreated, 2); //Adiciona duas Horas ao Horário
    if (isAfter(Date.now(), compareDates)){ //Verifica se o Token ainda é válido, comparando as datas
      throw new AppError('Token expirado');
    }

    //Altera a senha
    user.password = await this.hashProvider.generateHash(password);

    //Atualiza o cadastro no Banco
    this.usersRepository.save(user);
  }
}

export default ResetPasswordService;
