import AppError from '@shared/errors/AppError';
import { injectable, inject } from 'tsyringe'; //Injeção de Dependência
import User from '../infra/typeorm/entities/User';

import IHashProvider from '../providers/HashProvider/models/IHashProvider';
import IUserRepository from '../repositories/IUserRepository';

interface IRequest{
  user_id: string;
  name: string
  email: string;
  old_password?: string;
  password?: string;
}

@injectable()
class UpdateProfileService {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUserRepository,

    @inject('HashProvider')
    private hashProvider: IHashProvider,
  ){}

  public async execute({
    user_id,
    name,
    email,
    old_password,
    password,
  }: IRequest): Promise<User>{
    const user = await this.usersRepository.findById(user_id);
    if (!user){
      throw new AppError('Usuário não cadastrado!');
    }

    const userByEmail = await this.usersRepository.findByEmail(email);
    if (userByEmail && userByEmail.id !== user_id){
      throw new AppError('Email informado já está cadastrado!');
    }

    user.name = name;
    user.email= email;

    if (password && !old_password){
      throw new AppError('Não foi informado a senha antiga!');
    }

    if (password && old_password){
      const checkOldPassword = await this.hashProvider.compareHash(
        old_password,
        user.password,
      )
      if (!checkOldPassword){
        throw new AppError('A senha antiga informada é inválida!');
      }

      user.password = await this.hashProvider.generateHash(password);
    }

    return this.usersRepository.save(user);
  }
}

export default UpdateProfileService;
