import path from 'path';
import crypto from 'crypto';
import multer, { StorageEngine } from 'multer';

const tmpFolder = path.resolve(__dirname, '..', '..', 'tmp');

interface IUploadConfig {
  driver: 's3' | 'disk',

  tmpFolder: string,
  uploadsFolder: string,

  multer: {
    storage: StorageEngine,
  }

  config: {
    disk: {},
    aws: {
      bucket: string,
    }
  }
}

export default {
  driver: process.env.STORAGE_DRIVER,

  tmpFolder,
  uploadsFolder: path.resolve(tmpFolder, 'uploads'),

  multer: {
    storage: multer.diskStorage({
      //Local onde as imagens serão salvas. A princípio as imagens serão salvas na
      //pasta 'tmp' dentro da própria aplicação
      destination: tmpFolder,
      filename(request, file, callback){
        //Define o nome do arquivo, encriptografando para poder salvar e assim
        //não subscrever arquivos já salvos
        const fileHash = crypto.randomBytes(10).toString('hex');
        const fileName = `${fileHash}-${file.originalname}`;

        //Caso ocorra algum erro, pode ser tratado e retornado via callbak
        //na primeira posição, caso contrário, será salvo o arquivo com o nome definido acima
        return callback(null, fileName);
      }
    }),
  },

  config: {
    disk: { },
    aws: {
      bucket: 'go-berber'
    }
  }

} as IUploadConfig;
