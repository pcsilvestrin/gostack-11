export default {
  jwt: {
    secret: process.env.APP_SECRET || 'default', //Carrega do arquivo de Variáveis de Ambiente
    expiresIn: '1d',
  }
}
