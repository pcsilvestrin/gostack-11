/*Define que só aceita dois tipos de Driver sendo:
ethereal - Para ambiente de Desenvolvimento
ses - Amazon SES, para Ambiente de Produção
*/
interface IMailConfig {
  driver: 'ethereal' | 'ses';

  defaults: {
    from: {
      email: string;
      name : string;
    }
  }
}

export default {
  //Se existir a variável de ambiente, senão utiliza o 'ethereal'
  driver: process.env.MAIL_DRIVER || 'ethereal',

  defaults: {
    from: {
      //Deve ser informado o email configurado na Amazon SES, como email que emite email
      email: 'pcsilvestrin@hotmail.com',
      name: 'Paulo Cesar Silvestrin'
    }
  }
} as IMailConfig;
