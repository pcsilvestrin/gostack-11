import fs from 'fs';
import aws, { S3 } from 'aws-sdk';
import path from 'path';
import mime from 'mime';
import uploadConfig from '@config/upload';

import IStorageProvider from "../models/IStorageProvider";

export default class S3StorageProvider implements IStorageProvider {
  private client: S3;

  constructor(){
    this.client = new aws.S3({
      region: 'us-east-1',
    })
  }

  public async saveFile(file: string): Promise<string> {
    const originalPath = path.resolve(uploadConfig.tmpFolder, file);

    const contentType = mime.getType(originalPath);
    if (!contentType){
      throw new Error('file not found');
    }

    const fileContent = await fs.promises.readFile(originalPath);

    await this.client.putObject({
      Bucket: uploadConfig.config.aws.bucket, //Nome do Bucket criado na AWS
      Key: file,           //Nome do Arquivo
      ACL: 'public-read',  //Define a Permissão, se é publico, leitura e gravação, etc...
      Body: fileContent,   //Arquivo que será realizado o Upload
      ContentType: contentType,
    }).promise();

    //Após salvar na AWS, é excluido a imagem do Disco
    await fs.promises.unlink(originalPath);

    return file;
  }

  public async deleteFile(file: string): Promise<void> {
    await this.client.deleteObject({
      Bucket: uploadConfig.config.aws.bucket, //Nome do Bucket criado na AWS
      Key: file,           //Nome do Arquivo
    }).promise();
  }
}
