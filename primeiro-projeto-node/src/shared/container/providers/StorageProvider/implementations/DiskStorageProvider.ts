import fs from 'fs';
import path from 'path';
import uploadConfig from '@config/upload';

import IStorageProvider from "../models/IStorageProvider";

export default class DiskStorageProvider implements IStorageProvider {
  public async saveFile(file: string): Promise<string> {

    //Joga o arquivo da Pasta TMP para a pasta TMP/UPLOAD
    await fs.promises.rename(
      path.resolve(uploadConfig.tmpFolder, file),
      path.resolve(uploadConfig.uploadsFolder, file),
    );

    return file;
  }

  public async deleteFile(file: string): Promise<void> {
    const filePath = path.resolve(uploadConfig.uploadsFolder, file);

    try {
      //Verifica se existe o arquivo na pasta TMP/UPLOAD
      await fs.promises.stat(filePath);
    } catch {
      //Se cair aqui, o arquivo NÃO existe, não precisa excluir
      return;
    }

    //Exclui o arquivo da pasta TMP/UPLOAD
    await fs.promises.unlink(filePath);
  }
}
