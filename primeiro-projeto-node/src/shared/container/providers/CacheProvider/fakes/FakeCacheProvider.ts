import ICacheProvider from '../models/ICacheProvider';

interface ICacheData {
  [key: string]: string;
}

export default class FakeCacheProvider implements ICacheProvider {
  private cache: ICacheData = {};

  public async save(key: string, value: any): Promise<void>{
    this.cache[key] = JSON.stringify(value);
  }

  public async recover<T>(key: string): Promise<T | null>{
    //Carrega o Conteúdo salvo no Redis
    const data = this.cache[key];

    if (!data) {
      return null;
    }
    //Como foi salvo em formato JSON, o conteúdo deve ser convertido para String
    const parseData = JSON.parse(data) as T;
    return parseData;
  }

  //Deletar os Registros apartir de um prefixo
  public async invalidatePrefix(prefix: string): Promise<void> {
    delete this.cache[prefix];
  }

  public async invalidate(key: string): Promise<void>{
    const keys = Object.keys(this.cache).filter(key => {
      key.startsWith(`${key}:`);
    });

    keys.forEach(key => {
      delete this.cache[key];
    })
  } //Deletar
}
