import Redis, { Redis as RedisClient } from 'ioredis';
import cacheConfig from '@config/cache';
import ICacheProvider from '../models/ICacheProvider';

export default class RedisCacheProvider implements ICacheProvider {
  private client: RedisClient;
  constructor(){
    this.client = new Redis(cacheConfig.config.redis);
  }

  public async save(key: string, value: any): Promise<void>{
    this.client.set(key, JSON.stringify(value));
  }

  public async recover<T>(key: string): Promise<T | null>{
    //Carrega o Conteúdo salvo no Redis
    const data = await this.client.get(key);

    if (!data) {
      return null;
    }
    //Como foi salvo em formato JSON, o conteúdo deve ser convertido para String
    const parseData = JSON.parse(data) as T;
    return parseData;
  }

  //Deletar os Registros apartir de um prefixo
  public async invalidatePrefix(prefix: string): Promise<void> {
    //Carrega todas as chaves que estão dentro do prefix, prefix seria um nó
    //nesse caso está sendo pego todos os sub nós do nó principal
    const keys = await this.client.keys(`${prefix}:*`);

    //Para executar vários comandos ao mesmo tempo, como se criasse uma Transaction
    const pipeline = this.client.pipeline();

    //Percorre as chaves, realizando a exclusão
    keys.forEach(key => {
      pipeline.del(key);
    });
    //Comita a 'Transaction'
    await pipeline.exec();
  }

  public async invalidate(key: string): Promise<void>{
    await this.client.del(key);
  } //Deletar
}
