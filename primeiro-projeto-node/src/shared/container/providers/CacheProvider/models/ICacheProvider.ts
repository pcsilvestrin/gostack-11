export default interface ICacheProvider {
  save(key: string, value: any): Promise<void>;
  recover<T>(key: string): Promise<T | null>;  //Recarregar
  invalidate(key: string): Promise<void>;      //Deletar
  invalidatePrefix(prefix: string): Promise<void>; //Deletar os Registros apartir de um prefixo
}
