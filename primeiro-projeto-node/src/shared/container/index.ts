import { container } from 'tsyringe';

import '@modules/users/providers';
import './providers';

import IAppointmentsRepository from '@modules/appointments/repositories/IAppointmentsRepository';
import AppointmentsRepository from '@modules/appointments/infra/typeorm/repositories/AppointmentsRepository';

import IUsersRepository from '@modules/users/repositories/IUserRepository';
import UsersRepository from '@modules/users/infra/typeorm/repositories/UsersRepository';

import IUserTokenRepository from '@modules/users/repositories/IUserTokensRepository';
import UserTokenRepository from '@modules/users/infra/typeorm/repositories/UserTokensRepository';

import INotificationRepository from '@modules/notifications/repositories/INotificationRepository';
import NotificationsRepository from '@modules/notifications/infra/typeorm/repositories/NotificationsRepository';


//container.registerSingleton - Cria UMA instância do objeto para toda a aplicação
//container.register - Para cada arquivo que INJETAR a DEPENDENCIA criasse UMA instância do objeto
//IAppointmentsRepository - Obriga que ao Injetar a Dependência, o obj esperado é do tipo IAppointmentsRepository
container.registerSingleton<IAppointmentsRepository>(
  'AppointmentsRepository',  //Chave que deve ser utilizada na Injeção da Dependência
  AppointmentsRepository     //Define o objeto que será Instânciado
)

container.registerSingleton<IUsersRepository>(
  'UsersRepository',
  UsersRepository
)

container.registerSingleton<IUserTokenRepository>(
  'UserTokensRepository',
  UserTokenRepository
)

container.registerSingleton<INotificationRepository>(
  'NotificationsRepository',
  NotificationsRepository
)
