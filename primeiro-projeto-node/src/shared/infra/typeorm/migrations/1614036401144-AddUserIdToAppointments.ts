import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class AddUserIdToAppointments1614036401144 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    /*Cria a nova coluna*/
    await queryRunner.addColumn('appointments', new TableColumn({
      name: 'user_id',
      type: 'uuid',
      isNullable: true, /*Permite Null*/
    }));

    /*Gera a Foreign Key*/
    await queryRunner.createForeignKey('appointments', new TableForeignKey({
      name: 'AppointmentUser',   /*Define o Nome da ForeingKey*/
      columnNames: ['user_id'],  /*Nome da coluna da tabela USERS que é a chave estrangeira*/
      referencedColumnNames: ['id'], /*A chave estrangeira será referênciada pelo campo ID do Usuario*/
      referencedTableName: 'users',  /*Tabela referenciada*/
      onDelete: 'SET NULL', /*Ao deletar o CADASTRO DO USUÁRIO, será setado null no campo provider_id*/
      onUpdate: 'CASCADE',  /*Ao alterar o ID no CADASTRO DO USUÁRIO, será atualizado o id em todos os registros vinculados*/
    }));
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('appointments', 'AppointmentUser');
    await queryRunner.dropColumn('appointments', 'user_id');
  }
}
