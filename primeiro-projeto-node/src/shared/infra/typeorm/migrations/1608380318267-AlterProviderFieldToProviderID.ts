import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export default class AlterProviderFieldToProviderID1608380318267 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      /*Dropa a coluna*/
      await queryRunner.dropColumn('appointments', 'provider');

      /*Cria a nova coluna*/
      await queryRunner.addColumn('appointments', new TableColumn({
        name: 'provider_id',
        type: 'uuid',
        isNullable: true, /*Permite Null*/
      }));

      /*Gera a Foreign Key*/
      await queryRunner.createForeignKey('appointments', new TableForeignKey({
        name: 'AppointmentProvider',   /*Define o Nome da ForeingKey*/
        columnNames: ['provider_id'],  /*Nome da coluna da tabela USERS que é a chave estrangeira*/
        referencedColumnNames: ['id'], /*A chave estrangeira será referênciada pelo campo ID do Usuario*/
        referencedTableName: 'users',  /*Tabela referenciada*/
        onDelete: 'SET NULL', /*Ao deletar o CADASTRO DO USUÁRIO, será setado null no campo provider_id*/
        onUpdate: 'CASCADE',  /*Ao alterar o ID no CADASTRO DO USUÁRIO, será atualizado o id em todos os registros vinculados*/
      }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropForeignKey('appointments', 'AppointmentProvider');

      await queryRunner.dropColumn('appointments', 'provider_id');

      await queryRunner.addColumn('appointments', new TableColumn({
        name: 'provider',
        type: 'varchar',
      }));
    }
}
