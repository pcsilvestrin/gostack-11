import {MigrationInterface, QueryRunner, Table} from "typeorm";

export default class CreateAppointments1608301150215 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'appointments',
                columns: [
                    {
                        name: 'id',
                        type: 'uuid',
                        isPrimary: true,
                        generationStrategy: 'uuid',
                        default: 'uuid_generate_v4()',
                    },
                    {
                        name: 'provider',
                        type: 'varchar',
                        isNullable: false,
                    },
                    {
                        name: 'date',
                        type: 'timestamp with time zone',
                        isNullable: false,
                    },
                    {
                      name: 'created_at', /*Armazena automaticamente a Data e Hora que foi INCLUIDO*/
                      type: 'timestamp',
                      default: 'now()',
                    },
                    {
                      name: 'updated_at', /*Armazena automaticamente a Data e Hora que foi ALTERADO*/
                      type: 'timestamp',
                      default: 'now()',
                    }
                ]
            })
        );
    }
    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('appointments');
    }
}
