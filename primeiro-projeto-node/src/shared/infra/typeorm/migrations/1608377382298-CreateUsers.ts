import {MigrationInterface, QueryRunner, Table} from "typeorm";

export default class CreateUsers1608377382298 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'users',
        columns: [
          {
            name: 'id',
            type: 'uuid',
            isPrimary: true,
            generationStrategy: 'uuid',
            default: 'uuid_generate_v4()', /*O valor do Campo será gerado automaticamente pelo uuid*/
          },
          {
            name: 'name',
            type: 'varchar',
          },
          {
            name: 'email',
            type: 'varchar',
            isUnique: true, /*Define que o campo é unico*/
          },
          {
            name: 'password',
            type: 'varchar',
          },
          {
            name: 'created_at', /*Armazena automaticamente a Data e Hora que foi INCLUIDO*/
            type: 'timestamp',
            default: 'now()',
          },
          {
            name: 'updated_at', /*Armazena automaticamente a Data e Hora que foi ALTERADO*/
            type: 'timestamp',
            default: 'now()',
          }
        ]
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('users');
  }
}
