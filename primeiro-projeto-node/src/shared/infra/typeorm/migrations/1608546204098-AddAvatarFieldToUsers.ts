import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export default class AddAvatarFieldToUsers1608546204098 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    /*Criando a coluna 'avatar' dentro da tabela 'users' */
    await queryRunner.addColumn(
      'users',
      new TableColumn({
        name: 'avatar',
        type: 'varchar',  /*No banco, deve ser salvo apenas o caminho da Imagem*/
        isNullable: true, /*Pode permitir null, pois já existe registros gerados*/
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropColumn('users', 'avatar');
  }
}
