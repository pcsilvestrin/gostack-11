import { Request, Response, NextFunction } from 'express';
import redis, { RedisClient } from 'redis';
import { RateLimiterRedis } from 'rate-limiter-flexible';
import AppError from '@shared/errors/AppError';

const redisClient = redis.createClient({
  host: process.env.REDIS_HOST,
  port: Number(process.env.REDIS_PORT),
  password: process.env.REDIS_PASS || undefined,
});

const limiter = new RateLimiterRedis({
  storeClient: redisClient, //Onde será salvo a quantidade de Acessos
  keyPrefix: 'ratelimit', //Qual será a chave do registro no Redis
  points: 5,   //Quantas consultas serão permitidas, por tempo
  duration: 1, //Define o Tempo, no caso é permitido no máximo 5 requisições por Segundo
});

export default async function rateLimiter(
  request: Request,
  response: Response,
  next: NextFunction,
): Promise<void> {

  try {
    //Verifica se o IP do Usuário que fez a Requisição,
    //execedeu o Número de requisições por Segundo
    await limiter.consume(request.ip);

    return next();
  } catch (error) {
    throw new AppError('Excedeu o número de Requisições por Segundo!', 429);
  }
}
