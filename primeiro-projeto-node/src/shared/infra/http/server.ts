import 'reflect-metadata';
import 'dotenv/config'; //Manipular as Variáveis de Ambiente

import express, { Request, Response, NextFunction } from 'express';
import cors from 'cors';
import { errors } from 'celebrate'; //Validação das Requisições
import 'express-async-errors';

import routes from './routes';
import uploadConfig from '../../../config/upload';
import rateLimiter from '../http/middlewares/rateLimiter'; //Evitando ataques DDoS
import AppError from '@shared/errors/AppError';

import "@shared/infra/typeorm";
import "@shared/container"; //Para injeção de Dependências

const app = express();

app.use(cors());
app.use(express.json());
app.use('/files', express.static(uploadConfig.uploadsFolder));

app.use(rateLimiter); //Só aplica nas rotas abaixo
app.use(routes);

app.use(errors()); //Celebrate

app.get('/', (request, response) => {
  return response.json({ message: 'Olá GoStack!' });
});


//Middleware Global Exception Handler
app.use((err: Error, request: Request, response: Response, next: NextFunction) => {

  if (err instanceof AppError){
    //Se o erro foi Tratado como um AppError, será retornado o erro no seguinte formado
    return response.status(err.statusCode).json({
      status: 'error',
      message: err.message,
    });
  }

  //Se cair aqui, siginifica que o erro que ocorreu não foi tratado
  console.log(err);

  return response.status(500).json({
    status: 'error',
    message: 'Internal server error',
  });
});

app.listen(3333, () => {
  console.log('Servidor Rodando na porta 3333!');
});
