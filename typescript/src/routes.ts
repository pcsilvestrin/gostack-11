import { Request, Response } from 'express';
import createUser from './services/CreateUser';

export function olaMundo(request: Request, response: Response){
  const user = createUser({
    email: 'pcsilvestrin@hotmail.com',
    password: 'teste',
    techs: [
      'Delphi',
      'C#',
      'Java',
      {title: 'React JS', experience: 10},
    ]
  });

  return response.json({message: 'Olá Mundo!'});
};