interface TechObject{
  title: string;
  experience: number;
}

interface CreateUserData{
  name?: string; //? - Define que o campo não é obrigatório
  email: string;
  password: string;
  techs: Array<string | TechObject>; //Cria uma prop de Array que recebe strings e objetos do tipo TechObject
}

//Desestrtura o objeto passado por parametro, pegando apenas os campos que serão utilizados
export default function createUser({ name, email, password }: CreateUserData){
  const user = {
    name, 
    email,
    password,
  }
  return user;
}