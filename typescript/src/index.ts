import express, { response } from 'express';
import { olaMundo } from './routes';

const app = express();

app.get('/', olaMundo);

app.listen(3333);