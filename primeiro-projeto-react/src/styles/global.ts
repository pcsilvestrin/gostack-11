import { createGlobalStyle } from 'styled-components';

import gitHubBackground from '../assets/github-background.svg';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    background: #F0F0F5 url(${gitHubBackground}) no-repeat 70% top;
    -webkit-font-smoothing: antialiased; //Melhora a renderização da Fonte no Chrome
  }

  body, input, button {
    //Roboto, fonte importada do google fonts, deve ser feita a importação
    //no arquivo index, da pasta 'public'
    font: 16px Roboto, sans-serif
  }

  //Elemento principal, onde será apresentado todos os componentes, 
  //esta no arquivo index, da pasta 'public'
  #root { 
    max-width: 960px; //Tamanho MAXIMO, de acordo com o maior elemento que será apresentado
    margin: 0 auto;
    padding: 40px 20px;
  }

  button {
    cursor: pointer; //Por padrão não vem o cursor ao passar sobre os botões
  }
`;