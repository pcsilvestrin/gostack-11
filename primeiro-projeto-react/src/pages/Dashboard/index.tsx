import React, { useState, useEffect, FormEvent } from 'react';
import { FiChevronRight } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import api from '../../services/api';

import { Title, Form, Repositories, Error } from './styles';
import logoImg from '../../assets/logo.svg';

interface Repository {
  full_name: string;
  description: string;
  owner: {
    login: string;
    avatar_url: string;
  };  
}

const Dashboard: React.FC = () => { //React.FC - Compoente escrito em formato de Função
  const [newRepo, setNewRepo] = useState('');   //Armazena o Repositório informado no Input
  const [inputError, setInputError] = useState('');
  const [repositories, setRepositories] = useState<Repository[]>(() => {
    const storagedRepositories = localStorage.getItem('@GithubExplorer:repositories');

    if (storagedRepositories){
      return JSON.parse(storagedRepositories);
    }

    return [];
  });

  //Sempre que ocorrer uma mudança no repositories, será salvo os repositorires no storage
  useEffect(() => {
    localStorage.setItem('@GithubExplorer:repositories', JSON.stringify(repositories));
  }, [repositories]);

  async function handleAddRepository(event: FormEvent<HTMLFormElement>): Promise<void>{
    event.preventDefault(); //O event é o onSubmit, a linha abaixo desabilita o evento de atualizar a página após o Submit do Form

    if (!newRepo){
      setInputError('Digite o autor/nome do repositório!');
      return;
    }
   
    try {
      //Faz a busca dos repositórios na API já tipando o Retorno como Repository
      const response = await api.get<Repository>(`repos/${newRepo}`);
      const repository = response.data;

      setRepositories([...repositories, repository]);
      setNewRepo(''); //Limpa o Input      
      setInputError('');
    } catch (err) {
      setInputError('Erro na busca por esse repositório!');
    }
  }

  return (
  <>
    <img src={logoImg} alt="Github Explorer" />
    <Title>Explore repositórios no GitHub</Title>

    {/*onSubmit - Ao clicar em Pesquisar, será disparado o evento onSubmit
    que por sua vez, chama a função 'handleAddRepository'*/}
    <Form hasError={!!inputError} onSubmit={handleAddRepository}> 
      <input  
        value={newRepo} //Seta o valor de NewRepo para o Input
        onChange={(e) => setNewRepo(e.target.value)} //Atualiza o valor de NewRepo sempre que o input for alterado
        placeholder="Digite o nome do Repositório"
      />
      <button type="submit">Pesquisar</button>
    </Form>

    {/*Só APRESENTA o COMPONENTE do ERRO quando o 'inputError' possuir algum conteúdo*/}
    {inputError && <Error>{inputError}</Error>}

    <Repositories>
      {/*Percorre a lista de repositorios e cria os elementos na tela*/}
      {repositories.map(repository => (
        <Link key={repository.full_name} to={`/repository/${repository.full_name}`}>
          <img 
            src={repository.owner.avatar_url} 
            alt={repository.owner.login}
          />
          <div>
            <strong>{repository.full_name}</strong>
            <p>{repository.description}</p>
          </div>
          <FiChevronRight size={20} />
        </Link>        
      ))}
    </Repositories>
  </> 
  );

}

export default Dashboard;