import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    //Zera as configurações padrões
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  body {
    background: #312E38;   //Insere a cor de Fundo da Aplicação
    color: #FFF;
    -webkit-font-smoothing: antialiased; //Melhora a renderização dos Textos
  }

  //Aplica a fonte Roboto Slab e o size padrão
  body, input, button {
    font-family: 'Roboto Slab', serif;
    font-size: 16px;
  }

  //Elementos NEGRITOS, aplica uma opacidade para que não fique muito forte
  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 500;
  }

  //Aplica o ponteiro ao passar com o mouse sobre um botão
  button {
    cursor: pointer;
  }
`;
