import React, { useCallback, useEffect } from 'react';

import { ToastMessage, useToast } from '../../../context/ToastContext';
import { FiAlertCircle, FiCheckCircle, FiInfo, FiXCircle } from 'react-icons/fi';

import { Container } from './styles';

interface ToastProps {
  message: ToastMessage;
  style: object;
}

const icons = {
  info: <FiInfo size={24} />,
  error: <FiAlertCircle size={24} />,
  success: <FiCheckCircle size={24} />,
};

const Toast: React.FC<ToastProps> = ({ message, style }) => {
  const { removeToast } = useToast();

  //Remover o Toast após 3 segundos
  useEffect(() => {
    const timer = setTimeout(() => {
      removeToast(message.id);
    }, 3000);

    //Se o componente for destruido antes do Tempo, é removido o Timer, para que não seja executado
    //A função retornada, será executada sempre que o objeto for destruido
    return () => {
      clearTimeout(timer);
    }
  }, [removeToast, message.id]);

  const handleRemoveToast = useCallback((id: string) => {
    removeToast(id);
  }, [removeToast]);

  return (
    <Container
      type={message.type}
      hasDescription={Number(!!message.description)}
      style={style}
    >
      {icons[message.type || 'info']} {/*Adiciona o Ícone*/}
      <div>
        <strong>{message.title}</strong>
        {message.description && <p>{message.description}</p>}
      </div>

      <button onClick={() => handleRemoveToast(message.id)} type="button">
        <FiXCircle size={18} />
      </button>
    </Container>
  );
}

export default Toast;
