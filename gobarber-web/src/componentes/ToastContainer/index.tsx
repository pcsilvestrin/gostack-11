import React from 'react';
import { useTransition } from 'react-spring';

import Toast from './Toast';
import { ToastMessage } from '../../context/ToastContext';

import { Container } from './styles';

interface ToastContainerProps {
  messages: ToastMessage[];
}

const ToastContainer: React.FC<ToastContainerProps> = ({ messages }) => {
  const messagesWithTransitions = useTransition(
    messages, /* Pega a lista de mensagens */
    (message) => message.id, /* Valor do elemento que diferencia dos demais */
    {
      from: { right: '-120%', opacity: 0 }, /* O Toast será criado Fora da TELA */
      enter:  { right: '0%', opacity: 1 },  /* Sua posição final será alinhado A DIREITA dentro da Tela */
      leave:  { right: '-120%', opacity: 0 }, /* Ao destruir o Toast, será alinhado FORA da Tela Novamente */
    }
  );

  return (
    <Container>
      {/*Percorre a lista de mensagens para gerar os Toasts*/}
      {messagesWithTransitions.map(({ item, key, props }) => (
        <Toast key={key} style={props} message={item} />
      ))};
    </Container>
  );
};

export default ToastContainer;
