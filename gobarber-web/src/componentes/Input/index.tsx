import React, { InputHTMLAttributes, useState, useEffect, useCallback, useRef } from 'react';
import { IconBaseProps } from 'react-icons';
import { FiAlertCircle } from 'react-icons/fi';
import { useField } from '@unform/core';

import { Container, Error } from './styles';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;  //Subscreve a propriedade NAME, tornando-a OBRIGATÓRIA
  containerStyle?: React.CSSProperties;
  icon?: React.ComponentType<IconBaseProps>; //Cria a propriedade 'icon', carregando todas as propriedades que um Icone possui, através do IconBaseProps
}

const Input: React.FC<InputProps> = ({
    name,
    containerStyle = {},
    icon: Icon,
    ...rest
}) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);
  const { fieldName, defaultValue, error, registerField } = useField(name);

  const handleInputOnFocus = useCallback (() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
     setIsFocused(false);

     //inputRef.current? - Antes de pegar o value, verifica se possui o corrent
     // !! - Converte para boolean, se o input possuir valor, será true
     setIsFilled(!!inputRef.current?.value);
  }, []);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref : inputRef.current, /* Cria a Referência do componente */
      path: 'value', /* Utiliza a referencia criada acima e captura o valor informado no Input */
    });
  }, [fieldName, registerField]);

  return (
    <Container style={containerStyle} isErrored={!!error} isFilled={isFilled} isFocused={isFocused}>
      { Icon && <Icon size={20} /> } {/*Se possuir icone informado*/}
      <input
        onFocus={handleInputOnFocus} //Quando setar o Foco no Input
        onBlur={handleInputBlur} //Quando o Foco sair do campo Input, executa a função handleInput
        defaultValue={defaultValue}
        ref={inputRef}
        {...rest}
      />

      {error && (
        <Error title={error}>
          <FiAlertCircle color="#c53030" size={20} />
        </Error>
       )}
    </Container>
  );
}

export default Input;
