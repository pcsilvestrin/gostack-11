import React from 'react';
import {
    Route as ReactDOMRoute,
    RouteProps as ReactDOMRouteProps,
    Redirect
  } from 'react-router-dom';

import { useAuth }  from '../context/AuthContext';

interface RouteProps extends ReactDOMRouteProps {
  isPrivate?: boolean;
  component: React.ComponentType;
}

const Route: React.FC<RouteProps> = ({ isPrivate = false, component: Component, ...rest }) => {
  const { user } = useAuth();

  return (
    <ReactDOMRoute
      {...rest}
      render={( ) => {
        return isPrivate === !!user ? (
          //Se a rota for PRIVADA e o usuário estiver autenticado, Abre o Componente passado por Parâmetro
          //Se a rota NÃO for PRIVADA e o usuário NÃO estiver autenticado, Abre o Componente passado por Parâmetro
          <Component />
        ) : (
          //Se a rota for privada e o usuário NÃO estiver autenticado, volta para a tela de Login
          //Se a rota NÃO for privada, abre a tela de Dashboard
          <Redirect to={{
            pathname: isPrivate ? '/' : 'dashboard',
            //state: { from: location }, //Mantém o Histórico de navegação, por mais que abra direto a tela de Dashboard
          }} />
        );
      }}
    />
  );
};

export default Route;
