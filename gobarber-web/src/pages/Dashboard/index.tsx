import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { isToday, format, isAfter } from 'date-fns';
import ptBR from 'date-fns/locale/pt-BR';
import DayPicker, { DayModifiers } from 'react-day-picker';
import 'react-day-picker/lib/style.css';

import { FiClock, FiPower } from 'react-icons/fi';
import { Container,
         Header,
         HeaderContent,
         Profile,
         Content,
         Schedule,
         NextAppointment,
         Section,
         Appointment,
         Calendar
         } from './styles';
import { useAuth } from '../../context/AuthContext';

import logoImg from '../../assets/logo.svg';
import api from '../../services/api';
import { parseISO } from 'date-fns/esm';
import { Link } from 'react-router-dom';

interface IMonthAvailabilityItem {
  day: number;
  available: boolean;
}

interface IAppointment {
  id: string;
  date: string;
  hourFormated: string;
  user: {
    name: string;
    avatar_url: string;
  }
}

const Dashboard: React.FC = () => {
  const { signOut, user } = useAuth();

  const [selectedDate, setSelectedDate] = useState(new Date()); //Dia Selecionado
  const [currentMonth, setCurrentMonth] = useState(new Date()); //Mês Selecionado

  const [monthAvailability, setMonthAvailability] = useState<IMonthAvailabilityItem[]>([]);

  const [appointments, setAppointments] = useState<IAppointment[]>([]);

  const handleDateChange = useCallback((day: Date, modifiers: DayModifiers) => {
    //Permite clicar sobre o dia, apenas se o dia estiver disponivel
    if (modifiers.available && !modifiers.disabled){
      setSelectedDate(day);
    }
  }, []);

  const handleMonthChange = useCallback((month: Date) => {
    setCurrentMonth(month);
    console.log(currentMonth);
  }, []);

  //Ao alterar o valor das variáveis monthAvailability ou user.id, será executado a Função
  useEffect(() => {
    api.get(`/providers/${user.id}/month-availability`, {
      params: {
        year: currentMonth.getFullYear(),
        month: currentMonth.getMonth() + 1,
      },
    }).then(response => {
      setMonthAvailability(response.data);
    });
  }, [currentMonth, user.id]);

  //Carrega os Agendamentos na API do Dia selecionado
  useEffect(() => {
    api.get<IAppointment[]>('/appointments/me', {
      params: {
        year: selectedDate.getFullYear(),
        month: selectedDate.getMonth() + 1,
        day: selectedDate.getDate(),
      }
    }).then(response => {
      //Cria o array appointmentsFormatted aparti dos Appointments retornados da API
      //adicionando valor para o campo hourFormated
      const appointmentsFormatted = response.data
        .map(appointment => {
          return {
            ...appointment,
            hourFormated: format(parseISO(appointment.date), 'HH:mm'),
          }
        });

      setAppointments(appointmentsFormatted);
    });
  }, [selectedDate]);

  //useMemo - Memoriza os valores, até que o valor da variável currentMonth ou monthAvailability seja alterado
  //Monta um array com os dias que não podem receber agendamento, para que seja desabilitado os dias no Calendário
  const disabledDays = useMemo(() => {
    const dates = monthAvailability
      .filter(monthDay => monthDay.available === false)
      .map(monthDay => {
        const year = currentMonth.getFullYear();
        const month = currentMonth.getMonth();

        return new Date(year, month, monthDay.day);
      });

      return dates;
  }, [currentMonth, monthAvailability])

  //Gera a descrição da Data ao selecionar no calendário
  const seelctedDateAsText = useMemo(() => {
    return format(selectedDate, "'Dia' dd 'de' MMM", {
      locale: ptBR,
    });
  }, [selectedDate]);

  const selectedWeekDay = useMemo(() => {
    return format(selectedDate, 'cccc', { locale: ptBR });
  }, [selectedDate]);

  const morningAppointments = useMemo(() => {
    return appointments.filter(appointment => {
      return parseISO(appointment.date).getHours() < 12;
    });
  }, [appointments]);

  const afternoonAppointments = useMemo(() => {
    return appointments.filter(appointment => {
      return parseISO(appointment.date).getHours() >= 12;
    });
  }, [appointments]);

  //Pega o próximo agendamento, apartir da Hora Atual
  const nextAppointment = useMemo(() => {
    return appointments.find(appointment => {
      isAfter(parseISO(appointment.date), new Date());
    });
  }, [appointments]);

  return (
      <Container>
        <Header>
          <HeaderContent>
            <img src={logoImg} alt="GoBarber" />
            <Profile>
              <img
                src={user.avatar_url}
                alt={user.name}
              />
              <div>
                <span>Bem Vindo</span>
                <Link to="/profile">
                  <strong>{user.name}</strong>
                </Link>
              </div>
            </Profile>

            <button type="button" onClick={signOut}>
              <FiPower />
            </button>
          </HeaderContent>
        </Header>

        <Content>
          <Schedule>
            <h1>Horários Agendados</h1>
            <p>
              {isToday(selectedDate) && <span>Hoje</span>}
              <span>{seelctedDateAsText}</span>
              <span>{selectedWeekDay}</span>
            </p>

            {isToday(selectedDate) && nextAppointment && (
              <NextAppointment>
                <strong>Atendimento a Seguir</strong>
                <div>
                  <img
                    src={nextAppointment.user.avatar_url}
                    alt={nextAppointment.user.name}
                  />

                  <strong>{nextAppointment.user.name}</strong>
                  <span>
                    <FiClock />
                    {nextAppointment.hourFormated}
                  </span>
                </div>
              </NextAppointment>
            )}

            <Section>
              <strong>Manhã</strong>
              {morningAppointments.length === 0 && (
                <p>Nenhum agendamento para este período</p>
              )}

              {morningAppointments.map(appointment => (
                <Appointment key={appointment.id}>
                  <span>
                    <FiClock />
                    {appointment.hourFormated}
                  </span>

                  <div>
                    <img
                      src={appointment.user.avatar_url}
                      alt={appointment.user.name}
                    />
                    <strong>{appointment.user.name}</strong>
                  </div>
                </Appointment>
              ))}
            </Section>
            <Section>
              <strong>Tarde</strong>
              {afternoonAppointments.length === 0 && (
                <p>Nenhum agendamento para este período</p>
              )}

              {afternoonAppointments.map(appointment => (
                <Appointment key={appointment.id}>
                  <span>
                    <FiClock />
                    {appointment.hourFormated}
                  </span>

                  <div>
                    <img
                      src={appointment.user.avatar_url}
                      alt={appointment.user.name}
                    />
                    <strong>{appointment.user.name}</strong>
                  </div>
                </Appointment>
              ))}
            </Section>

          </Schedule>
          <Calendar>
            <DayPicker
              weekdaysShort={['D','S','T','Q','Q','S','S']} //Legenda dias da semana
              fromMonth={new Date()} //Só permite selecionar datas de Meses Futuros
              disabledDays={[{ daysOfWeek: [0, 6] }, ...disabledDays]} //Desabilita os dias que caem no Sabado e Domingo e os dias que não podem receber agendamento
              modifiers={{
                //Aplica o CSS da classe 'available' para os dias de Segunda, Terça, Quarta, Quinta, Sexta
                available: { daysOfWeek: [1, 2, 3, 4, 5] },
              }}
              selectedDays={selectedDate} //Aplica o efeito no dia Selecionado
              onDayClick={handleDateChange}
              onMonthChange={handleMonthChange}
              months={[
                'Janeiro',
                'Fevereiro',
                'Março',
                'Abril',
                'Maio',
                'Junho',
                'Julho',
                'Agosto',
                'Setembro',
                'Outubro',
                'Novembro',
                'Dezembro',
              ]}
            />
          </Calendar>
        </Content>
      </Container>
    );
}

export default Dashboard;
