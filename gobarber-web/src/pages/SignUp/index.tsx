import React, { useCallback, useRef } from 'react';
import { FiUser, FiArrowLeft, FiMail, FiLock } from 'react-icons/fi';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/web';
import * as Yup from 'yup';
import getValidationErrors from '../../utils/getValidationErrors';
import { Link, useHistory } from 'react-router-dom';

import api from '../../services/api';
import { useToast } from '../../context/ToastContext';

import logoImg from '../../assets/logo.svg';
import { Container, Content, AnimationContainer, Background } from './styles';

import Input from '../../componentes/Input';
import Button from '../../componentes/Button';

interface SignUpFormData {
  name: string;
  email: string;
  password: string;
}

const SignUp: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { addToast } = useToast();
  const history = useHistory();

  const handleSubmit = useCallback(async (data: SignUpFormData) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        name: Yup.string().required('Nome obrigatório'),
        email: Yup.string().required('Email obrigatório').email('Digite um e-mail válido'),
        password: Yup.string().min(6, 'No mínimo 6 digitos'),
      });

      await schema.validate(data, {
        abortEarly: false, /*Para retornar TODOS os ERROS de uma única vez*/
      });

      //Envia para a API os dados do usuário para que possa ser salvo
      await api.post('/users', data);

      history.push('/'); //Volta para a tela de Login

      addToast({
        type: 'success',
        title: 'Cadastro Realizado',
        description: 'Você já pode fazer seu logon no GoBarber!',
      });
    } catch (err) {
      if (err instanceof Yup.ValidationError){   //Verifica se o Erro ocorreu na validação utilizando o schema do Yup
        const errors = getValidationErrors(err); //Trata os ERROS e carrega as mensagens nos Inputs
        formRef.current?.setErrors(errors);
      }

      addToast({
        type: 'error',
        title: 'Erro no Cadastro',
        description: 'Ocorreu um erro ao realizar o cadastro do usuário.'
      });
    }
  }, [addToast, history]);

  return (
    <Container>
      <Background />
        <Content>
          <AnimationContainer>
            <img src={logoImg} alt="GoBarber"/>

            <Form ref={formRef} onSubmit={handleSubmit}>
              <h1>Faça seu Cadastro</h1>
              <Input name="name" icon={FiUser} placeholder="Nome"/>
              <Input name="email" icon={FiMail} placeholder="E-Mail"/>
              <Input name="password" icon={FiLock} type="password" placeholder="Senha" />
              <Button type="submit">Cadastrar</Button>
            </Form>
            <Link to="/">
              <FiArrowLeft />
              Voltar para Logon
            </Link>
          </AnimationContainer>
        </Content>
      </Container>
  );
}

export default SignUp;
