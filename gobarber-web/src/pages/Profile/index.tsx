import React, { ChangeEvent, useCallback, useRef } from 'react';
import { FiUser, FiMail, FiLock, FiCamera, FiArrowLeft } from 'react-icons/fi';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/web';
import * as Yup from 'yup';
import getValidationErrors from '../../utils/getValidationErrors';

import api from '../../services/api';
import { useToast } from '../../context/ToastContext';

import { Container, Content, AvatarInput } from './styles';

import Input from '../../componentes/Input';
import Button from '../../componentes/Button';
import { useHistory } from 'react-router';
import { useAuth } from '../../context/AuthContext';
import { Header } from '../Dashboard/styles';
import { Link } from 'react-router-dom';

interface ProfileFormData {
  name: string;
  email: string;
  password: string;
  old_password: string;
  password_confirmation: string;
}

const Profile: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { addToast } = useToast();
  const history = useHistory();

  const { user, updateUser } = useAuth();

  const handleSubmit = useCallback(async (data: ProfileFormData) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        name: Yup.string().required('Nome obrigatório'),
        email: Yup.string().required('Email obrigatório').email('Digite um e-mail válido'),

        old_password: Yup.string(),

        password: Yup.string().when('old_password', {
          is: (val: string) => !!val.length, //Verifica se 'old_password' possui valor
          then: Yup.string().required('Campo obrigatório!'), //Se 'old_password' possuir valor, 'password' é Obrigatório
          otherwise: Yup.string(), //SENÃO não é obrigatorio
        }),

        password_confirmation: Yup.string().when('old_password', {
          is: (val: string) => !!val.length, //Verifica se 'old_password' possui valor
          then: Yup.string().required('Campo obrigatório!'), //Se 'old_password' possuir valor, 'password' é Obrigatório
          otherwise: Yup.string(), //SENÃO não é obrigatorio
        })
        .oneOf([Yup.ref('password'), null], 'Confirmação Incorreta!'), //Verifica se foi informado a mesma senha
      });

      await schema.validate(data, {
        abortEarly: false, /*Para retornar TODOS os ERROS de uma única vez*/
      });

      const { name, email, password, old_password, password_confirmation } = data;

      //Gera um novo Objeto apartir de DOIS OBJETOS, quando for informado o old_password
      //na requisição será enviado os campos name, email, password, old_password, password_confirmation,
      //caso contrário será enviado apenas o name e o email
      const formData = {
        name,
        email,
        ...(old_password
          ? {
            password,
            old_password,
            password_confirmation
          } :
            {})
      }

      console.log(formData);

      //Envia para a API os dados do usuário para que possa ser atualizado o Perfil
      const response = await api.put('/profile', formData);
      updateUser(response.data); //Atualiza os dados do usuário logado

      history.push('/dashboard'); //Volta para a tela de Login

      addToast({
        type: 'success',
        title: 'Perfil Atualizado',
        description: 'Suas informações de Perfil foram Atualizadas!',
      });
    } catch (err) {
      if (err instanceof Yup.ValidationError){   //Verifica se o Erro ocorreu na validação utilizando o schema do Yup
        const errors = getValidationErrors(err); //Trata os ERROS e carrega as mensagens nos Inputs
        formRef.current?.setErrors(errors);
      }

      addToast({
        type: 'error',
        title: 'Erro na atualização de perfil',
        description: 'Ocorreu um erro na tentativa de atualizar o perfil.'
      });
    }
  }, [addToast, history]);

  //Atualiza o Avatar
  const handleAvatarChange = useCallback((e: ChangeEvent<HTMLInputElement> ) => {
    //Verifica se foi selecionado o Avatart
    if (e.target.files) {
      //Cria o objeto para enviar na requisição
      const data = new FormData();

      //Para atualizar o Avatar, é necessário enviar esses dados na requisição
      data.append('avatar', e.target.files[0]);

      //Executa a requisição na API, para atualizar o Avatar
      api.patch('/users/avatar', data).then(response => {

        //Pega o retorno e atualiza os dados do Usuário logado
        updateUser(response.data);

        addToast({
          type: 'success',
          title: 'Avatar Atualizado',
        });
      });
    }
  }, [addToast, updateUser]);

  return (
    <Container>
        <header>
          <div>
            <Link to="/dashboard">
              <FiArrowLeft />
            </Link>
          </div>
        </header>

        <Content>
          <Form
            ref={formRef}
            onSubmit={handleSubmit}
            initialData={{
              name: user.name,
              email: user.email,
            }}
          >
            <AvatarInput>
              <img src={user.avatar_url} alt={user.name} />
              <label htmlFor="avatar">
                <FiCamera />
                <input type="file" id="avatar" onChange={handleAvatarChange} />
              </label>
            </AvatarInput>

            <h1>Meu Perfil</h1>

            <Input name="name" icon={FiUser} placeholder="Nome"/>
            <Input name="email" icon={FiMail} placeholder="E-Mail"/>

            <Input containerStyle={{ marginTop: 24 }} name="old_password" icon={FiLock} type="password" placeholder="Senha Atual" />
            <Input name="password" icon={FiLock} type="password" placeholder="Nova Senha" />
            <Input name="password_confirmation" icon={FiLock} type="password" placeholder="Confirmação de Senha" />
            <Button type="submit">Confirmar mudanças</Button>
          </Form>
        </Content>
      </Container>
  );
}

export default Profile;
