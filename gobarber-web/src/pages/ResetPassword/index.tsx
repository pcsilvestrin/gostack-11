import React, { useRef, useCallback } from 'react';
import { FiLock } from 'react-icons/fi';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/web';
import * as Yup from 'yup';
import { useHistory, useLocation } from 'react-router-dom';

import { useToast } from '../../context/ToastContext'
import getValidationErrors from '../../utils/getValidationErrors';

import logoImg from '../../assets/logo.svg';
import { Container, Content, AnimationContainer, Background } from './styles';

import Input from '../../componentes/Input';
import Button from '../../componentes/Button';
import api from '../../services/api';

interface ResetPasswordFormData {
  password: string;
  password_confirmation: string;
}

const ResetPassword: React.FC = () => {
  const formRef = useRef<FormHandles>(null);

  const history = useHistory();
  const location = useLocation();

  const { addToast } = useToast();

  const handleSubmit = useCallback(async (data: ResetPasswordFormData) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        password: Yup.string().required('Senha obrigatória'),
        password_confirmation: Yup.string().oneOf(
          [Yup.ref('password'), null],
          'Confirmação incorreta',
        ),
      });

      await schema.validate(data, {
        abortEarly: false, /*Para retornar TODOS os ERROS de uma única vez*/
      });

      //Carrega as informações para enviar a requisição
      const {password, password_confirmation} = data;
      const token = location.search.replace('?token=', ''); //Carrega o Token passado na URL

      if (!token){
        throw new Error();
      }

      await api.post('/password/reset', {
        password,
        password_confirmation,
        token
      });

      history.push('/');
    } catch (err) {
      //Verifica se o Erro ocorreu na validação utilizando o schema do Yup
      if (err instanceof Yup.ValidationError){
        //Trata os ERROS e carrega as mensagens nos Inputs
        const errors = getValidationErrors(err);
        formRef.current?.setErrors(errors);

        //return;
      }

      addToast({
        type: 'error',
        title: 'Erro ao resetar senha',
        description: 'Ocorreu um erro ao Resetar Senha, tente novamente.'
      });
    }
  }, [addToast, history, location.search]);

  return (
    <Container>
      <Content>
        <AnimationContainer>
          <img src={logoImg} alt="GoBarber"/>

          <Form ref={formRef} onSubmit={handleSubmit}>
            <h1>Resetar Senha</h1>
            <Input name="password" icon={FiLock} type="password" placeholder="Nova Senha" />
            <Input name="password_confirmation" icon={FiLock} type="password" placeholder="Confirmação de Senha" />
            <Button type="submit">Alterar Senha</Button>
          </Form>
        </AnimationContainer>
      </Content>

      <Background />
    </Container>
  );
}

export default ResetPassword;
