import React, { createContext, useCallback, useContext, useState } from 'react';
import ToastContainer from '../componentes/ToastContainer';

import { v4 as uuid } from 'uuid';

export interface ToastMessage {
  id: string;
  type?: 'info' | 'success' | 'error' | undefined;
  title: string;
  description?: string;
}

interface ToastContextData {
  addToast(message: Omit<ToastMessage, 'id'>): void;
  removeToast(id: string): void;
}

const ToastContext = createContext<ToastContextData>({} as ToastContextData);

const ToastProvider: React.FC = ({ children }) => {
  const [messages, setMessages] = useState<ToastMessage[]>([]);

  const addToast = useCallback(({type, title, description} : Omit<ToastMessage, 'id'>) => {
    const id = uuid();

    const toast = {
      id,
      type,
      title,
      description,
    };

    //setMessages([...messages, toast]); OU
    //state - Corresponde a lista de Mensagens
    setMessages((state) => [...state, toast]);
  }, []);

  const removeToast = useCallback((id: string) => {
    //state - Corresponde a lista de Mensagens
    //Utilizando o 'filter' para que só carregue as mensagens onde o id não é igual ao passado por parâmetro
    setMessages((state) => state.filter((message) => message.id !== id));
  }, []);

  return (
    <ToastContext.Provider value={{ addToast, removeToast }}>
      {children}
      <ToastContainer messages={messages} />
    </ToastContext.Provider>
  );
};

function useToast(): ToastContextData {
  const context = useContext(ToastContext);

  if (!context){
    throw new Error('useToast não CONFIGURADO no app.tsx!');
  }

  return context;
};

export { ToastProvider, useToast };


