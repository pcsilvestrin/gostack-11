import React, { createContext, useCallback, useState, useContext } from 'react';
import api from '../services/api';

interface User {
  id: string;
  name: string;
  email: string;
  avatar_url: string;
}

interface AuthState {
  token: string;
  user: User;
}

interface SignInCredentials {
  email: string;
  password: string;
}

interface AuthContextData {
  user: User;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
  updateUser(user: User): void;
}

//{} as AuthContext - Inicia sempre vazio
const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>(() => {
    //Carrega os dados de Autenticação salvos no Storage
    const token = localStorage.getItem('@GoBarber:token');
    const user = localStorage.getItem('@GoBarber:user'); //Converte o User em String

    if (token && user) {
      //Seta o Token que será utilizado nas requisições para a API
      api.defaults.headers.authorization = `Bearer ${token}`;

      //Se possuir token e usuário salvos no Storage
      return { token, user: JSON.parse(user) };
    }

    return {} as AuthState; //Inicia o objeto vazio
  });

  const signIn = useCallback(async ({ email, password }) => {
    //Realiza a requisição de autenticação para a API, passando o Email e Senha
    //informados no componente SignIn
    const response = await api.post('sessions', {
      email,
      password
    });

    //Captura os dados de retornados na Autenticação e salva no Storage
    const { token, user } = response.data;
    localStorage.setItem('@GoBarber:token', token);
    localStorage.setItem('@GoBarber:user', JSON.stringify(user)); //Converte o User em String

    //Seta o Token que será utilizado nas requisições para a API
    api.defaults.headers.authorization = `Bearer ${token}`;

    setData({ token, user }); //Atualiza os dados de Autenticação armazenados no State
  }, []);

  const signOut = useCallback(() => {
    //Exclui as informações do Storage
    localStorage.removeItem('@GoBarber:token');
    localStorage.removeItem('@GoBarber:user');

    setData({} as AuthState);
  }, []);

  //Atualiza os Dados do usuário logado, após a atualização do Profile
  const updateUser = useCallback((user: User) => {
    localStorage.setItem('@GoBarber:user', JSON.stringify(user)); //Converte o User em String

    setData({
      token: data.token,
      user
    });
  }, [setData, data.token]);

  return (
    //Disponibiliza as informações user e signIn para qualquer componente que necessite
    //dessa forma, é possível obter os dados do usuário em qualquer parte da aplicação
    <AuthContext.Provider value={{ user: data.user, signIn, signOut, updateUser }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  //Verifica se o contexto foi criado, se o contexto abrange o componente que está chamando
  //assim como está descrito no tópico "3.20 API de Contexto - Compartilhando Estados entre vários Componentes"
  if (!context){
    throw new Error('useAuth não configurado para o APP');
  }

  return context;
}

export { AuthProvider, useAuth };
